#! /usr/bin/env python

import socket
import threading
import sys
import argparse

is_loop = True
connections =[]

def get_is_loop():
    global is_loop
    return is_loop

def set_is_loop(bflag):
    global is_loop
    is_loop = bflag

def add_connection(conn):
    global connections
    if conn not in connections:
        connections.append(conn)
        print("add new connection. now count is %d" % len(connections))

def del_connection(conn):
    global connections
    if conn in connections:
        connections.remove(conn)

def get_connections():
    global connections
    return connections


def handle_client(client_socket):
    recv_size = 4096
    while get_is_loop():
        r = client_socket.recv(recv_size)
        dr = r.decode('utf-8')
        print("<<<"+ dr)

        if dr == "--bye" or dr == "--Bye":
            del_connection(client_socket)
            client_socket.close()
            for conn in get_connections():
                print("send message to all client")
                r = "exit room another client."
                conn.send(r.encode('utf-8'))
            print("connection delete. now count is %d" % len(get_connections()))
            break
        else:
            for conn in get_connections():
                print("send message to all client")
                conn.send(r)
    
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--target", default="localhost", help="target ipAddress")
    parser.add_argument("-p", "--port", default = 8080, help="target port")
    args = parser.parse_args()

    bind_ip = args.target
    bind_port = args.port
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind((bind_ip, bind_port))
    max_socket_cnt = 5
    server.listen(max_socket_cnt)

    print("[*] Listening on %s:%d" % (bind_ip, bind_port))

    try:
        while get_is_loop():
            client, addr = server.accept()
            print("[*] Accepted connection from: %s:%d" % (addr[0], addr[1]))
        
            add_connection(client)
        
            client_handler = threading.Thread(target = handle_client, args = (client, ))
            client_handler.start()
    except KeyboardInterrupt:
        print("break loop")
        set_is_loop(False)
        for conn in get_connections:
            conn.close()
        sys.exit(1)
    print("THE END")



if __name__ == "__main__":
    main()
    