# -*- coding: utf-8 -*-

import socket
import os
import struct
import sys
import threading
import time
from netaddr import IPNetwork, IPAddress
from ctypes import *


from pelo_utils.p_base_number import p_base_number

host = "192.168.3.9"
subnet = "192.168.3.0/24"
magic_message = "PYTHONRULES!"
is_show_base_number = False

def udp_sender(subnet, magic_message):
    time.sleep(5)
    sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    for ip in IPNetwork(subnet):
        try:
            sender.sendto(magic_message.encode(), (str(ip), 65212))
            # print("send to %s" % str(ip))
        except:
            print("failed send udp packet to %s" % ip)
            pass
    print("Finish sending udp packet to PCs in same segment")

class IP(Structure):
    _fields_ =[
        ("ihl",          c_uint8, 4),
        ("version",      c_uint8, 4),
        ("tos",          c_uint8),
        ("len",          c_uint16),
        ("id",          c_uint16),
        ("offset",       c_uint16),
        ("ttl",          c_uint8),
        ("protocol_num", c_uint8),
        ("sum",          c_uint16),
        ("src",          c_uint32),
        ("dst",          c_uint32)
    ]

    def __new__(self, socket_buffer=None):
        return self.from_buffer_copy(socket_buffer)

    def __init__(self, socket_buffer=None):
        self.protocol_map ={1:"ICMP", 6:"TCP", 17:"UDP"}
        self.src_address = socket.inet_ntoa(struct.pack("<L",self.src))
        self.dst_address = socket.inet_ntoa(struct.pack("<L",self.dst))

        try:
            self.protocol = self.protocol_map[self.protocol_num]
        except:
            self.protocol = str(self.protocol_num)

    def print_logg(self):
        print("src: %s" % self.src)
        print("src type: %s" % type(struct.pack("<L", self.src)))
        print("dst: %s" % self.dst)
        print("ihl: %s" % self.ihl)
        print("offset: %s" % self.offset)


class ICMP(Structure):
    _fields_=[
        ("type",         c_uint8),
        ("code",         c_uint8),
        ("checksum",     c_uint16),
        ("unused",       c_uint16),
        ("next_hop_mtu", c_uint16)
    ]

    def __new__(self, socket_buffer):
        return self.from_buffer_copy(socket_buffer)

    def __init__(self, socket_buffer):
        pass


if os.name == "nt":
    socket_protocol = socket.IPPROTO_IP
else:
    socket_protocol = socket.IPPROTO_ICMP

sniffer = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket_protocol)
sniffer.bind((host, 0))
sniffer.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)

if os.name == "nt":
    sniffer.ioctl(socket.SIO_RCVALL, socket.RCVALL_ON)

t = threading.Thread(target= udp_sender, args=(subnet, magic_message))
t.start()

try:
    while True:
        raw_buffer = sniffer.recvfrom(65565)[0]

        # generate ip header
        ip_header = IP(raw_buffer[0:20])

        # if IPAddress(ip_header.src_address) in IPNetwork(subnet) and not ip_header.src_address == host:
        #     print("Protocol: %s %s -> %s" 
        #       % (ip_header.protocol, ip_header.src_address, ip_header.dst_address))

        if ip_header.protocol == "ICMP":
            offset = ip_header.ihl * 4
            buf = raw_buffer[offset: offset + sizeof(ICMP)]
            # genetarte icmp header
            icmp_header = ICMP(buf)
        #     if not ip_header.src_address == host:
        #         rr = "type:{0} code:{1}".format(icmp_header.type, icmp_header.code)
        #         print("Host Up: %s from %s %s" % (ip_header.dst_address,ip_header.src_address, rr))
        #     # if icmp_header.code == 3 and icmp_header.type == 3:
        #     #     if IPAddress(ip_header.src_address) in IPNetwork(subnet):
        #     #         print("Host Up: %s" % ip_header.src_address)

        #     # if IPAddress(ip_header.src_address) in IPNetwork(subnet):
        #     #     print("Hosttttttt Up: %s" % ip_header.src_address)

            if icmp_header.code == 3 and icmp_header.type == 3:
                if IPAddress(ip_header.src_address) in IPNetwork(subnet):
                    print("Host Up: %s" % ip_header.src_address)
                        
        #             # if raw_buffer[len(raw_buffer) - len(magic_message):].decode() == magic_message:
        #             #     print("Host Up: %s" % ip_header.src_address)

        # print binary or hexadecimal
        if is_show_base_number:
            base_num = p_base_number()
            base_num.binarydump(raw_buffer)
            base_num.hexdump(raw_buffer)
except KeyboardInterrupt:
    if os.name == "nt":
        sniffer.ioctl(socket.SIO_RCVALL, socket.RCVALL_OFF)



