# -*- coding: utf-8 -*-
from burp import IBurpExtender
from burp import IContextMenuFactory

from java.util import List, ArrayList
from java.net import URL
from javax.swing import JMenuItem

import socket
import urllib
import json
import re
import base64


subscription_key = "36e45cef19f34491882de98f237836d9"
search_url = "https://daijiro.cognitiveservices.azure.com/bing/v7.0/search"

class BurpExtender(IBurpExtender, IContextMenuFactory):
    def registerExtenderCallbacks(self, callbacks):
        self._callbacks = callbacks
        self._helpers = callbacks.getHelpers()
        self.context = None

        callbacks.setExtensionName("BHP Bing")
        callbacks.registerContextMenuFactory(self)
        return

    def createMenuItems(self, context_menu):
        self.context = context_menu
        menu_list = ArrayList()
        menu_list.add(JMenuItem("Send to Bing", actionPerformed = self.bing_menu))
        return menu_list

    def bing_menu(self, event):
        http_traffic = self.context.getSelectedMessages()
        print("%d requests highlighted" % len(http_traffic))

        for traffic in http_traffic:
            http_service = traffic.getHttpService()
            host = http_service.getHost()
            print("User selected host: %s" % host)
            self.bing_search(host)
        return
    
    def bing_search(self, host):
        is_ip = re.match("[0-9]+(?:\.[0-9]+){3}", host)
        if is_ip:
            ip_address = host
            domain = False
        else:
            ip_address = socket.gethostbyname(host)
            domain = True
        
        bing_query_string = "'ip:%s'" % ip_address
        self.bing_query(bing_query_string)

        if domain:
            bing_query_string = "'domain:%s'" % host
            self.bing_query(bing_query_string)
        return
    
    def bing_query(self, bing_query_string):
        print("bing_query_string: %s" % bing_query_string)
        quoted_query = urllib.quote(bing_query_string)
        print("bing_query_string_quote: %s" % quoted_query)
        http_request = "GET %s?$format=json&q=%s HTTP/1.1\r\n" % (search_url, quoted_query)
        http_request += "Host: %s" % search_url
        http_request += "Connection: close\r\n"
        http_request += "Ocp-Apim-Subscription-Key: %s" % subscription_key
        http_request += "User-Agent: Blackhat python\r\n\r\n"
        print("before send Request")
        json_body = self._callbacks.makeHttpRequest(search_url, 443, True, http_request).tostring()
        json_body = json_body.split("\r\n\r\n", 1)[1]
        print("send Request")
        try:
            r = json.loads(json_body)
            if len(r["d"]["results"]):
                for site in r["d"]["results"]:
                    print("*" * 100)
                    print(site['Title'])
                    print(site['Url'])
                    print(site['Description'])
                    print("*" * 100)
                    j_url = URL(site['Url'])
            if not self._callbacks.isInScope(j_url):
                print("Adding to Burp scope")
                self._callbacks.includeInScope(j_url)
        except:
            print("No results from Bing")
            pass

        return
    