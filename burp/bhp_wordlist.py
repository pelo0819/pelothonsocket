
from burp import IBurpExtender
from burp import IIntruderPayloadGeneratorFactory
from burp import IIntruderPayloadGenerator

from javax.swing import JMenuItem
from java.util import List, ArrayList
from java.net import URL

import re
from datetime import datetime
from html.parser import HTMLParser

class TagStripper(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.page_text = []
    
    def handle_date(self, data):
        self.page_text.append(data)
    
    def handle_comment(self, data):
        self.handle_data(data)
    
    def strip(self, html):
        self.feed(html)
        return " ".join(self.page_text)

