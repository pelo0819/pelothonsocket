
class Teacher():
    def __init__(self, callback):
        print("init")
        self.callback = callback

    
    def aaa(self):
        print("aaa method")
        bb = self.callback("tobita")
        print(bb)

    
    def bbb(self):
        print("bbb method")
        self.aaa()

def say_hello(name:str):
    print("hello %s!!!!" % name)
    return "good bye"

t = Teacher(say_hello)
# t.bbb()
t.aaa()