import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-t", "--target", default=3, help="target ipAddress")
parser.add_argument("-p", "--port", type=int,default=5 ,help="target port")

args=parser.parse_args()

print("target=%s, type=%s" % (args.target, type(args.target)))
print("port=%s, type=%s" % (args.port, type(args.port)))

try:
    while True:
        input(">>>")
except KeyboardInterrupt:
    print("byebye")
    exit(0)
