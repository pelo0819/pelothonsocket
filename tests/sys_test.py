# -*- coding: utf-8 -*-
from importlib.abc import Loader, MetaPathFinder
from importlib.machinery import ModuleSpec
from typing import Dict, List, Optional
from types import ModuleType
from pathlib import Path

import sys
import urllib.request, urllib.error
import threading
import os
import queue
import types

module_code = """
def one_plus(a):
    return a + 1

if __name__ == "__main__":
    one_plus(2)
"""

class PeloImporter():
    def __init__(self):
        print("__init__()")
        self.current_module_code = " "
    
    def find_module(self, fullname, path = None):
        self.current_module_code = module_code
        return self
    
    def load_module(self, name):
        print("name=%s" % name)
        module = types.ModuleType(name)
        exec(self.current_module_code, module.__dict__)
        sys.modules[name] = module
        return module


class PPeloImporter(Loader, MetaPathFinder):
    def __init__(self, module_code: str):
        print("__init")
        self.current_module_code = module_code

    def find_spec(self, fullname: str, path = None, 
            target: ModuleSpec = None) -> Optional[ModuleSpec]:
        print("find_spec name=%s" % fullname)
        self.name = fullname
        return ModuleSpec(fullname, self)

    def create_module(self, spec : ModuleSpec) -> Optional[ModuleType]:
        print("create_module")
        module = ModuleType(self.name)
        exec(self.current_module_code, module.__dict__)
        sys.modules[self.name] = module
        return module
    
    def exec_module(self, module: ModuleType):
        pass

importer = PPeloImporter(module_code)
sys.meta_path.insert(0, importer)
# sys.meta_path.insert(0, PeloImporter())

exec("import one_plus")
a = sys.modules["one_plus"].one_plus(3)
print("a=%d" % a)
# print("meta_path=%s" % type(sys.meta_path))

# for m in sys.meta_path:
#     print(m)

# md = "test2"
# if md in sys.modules:
#     print("Hit %s !!!" % md)

