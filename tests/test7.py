# -*- coding: utf-8 -*-
import sys
import urllib.request, urllib.error
import threading
import os
import queue

print("meta_path=%s" % type(sys.meta_path))

for m in sys.meta_path:
    print(m)

for m in sys.modules:
    print(m)