# -*- coding: utf-8 -*-
import urllib.request, urllib.error, urllib.parse
import re
from html.parser import HTMLParser


class TestHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == "input":
            print("hit start input tag")
            print(attrs)
    
    def handle_endtag(self, tag):
        if tag == "input":
            print("hit end input tag")


url = "http://192.168.3.50"

handler = urllib.request.HTTPHandler()
opener = urllib.request.build_opener(handler)

post = {
    'log': "pelo",
    'pwd': "0104Kimura"
}

data = urllib.parse.urlencode(post)
print(data)
print(data.encode('utf-8'))
with opener.open(url) as res:
    r = res.read()
    if len(r):
        print("[%d] => %s" % (res.code, url))
    print(r.decode('utf-8'))
    print(type(r))
    parser = TestHTMLParser()
    parser.feed(r.decode('utf-8'))