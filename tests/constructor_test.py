#! /usr/bin/env python
from ctypes import *

class test(Structure):
    _fields_ = [
        ("first", c_int8, 4),
        ("second", c_int8, 4)
    ]
    def __new__(self):
        return 5    
    def __init__(self):
        self.val2 = self

t = test()
print("val=%s" % t)
print()

digit = 151234752