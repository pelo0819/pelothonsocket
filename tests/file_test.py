class FileAnalyzer:
    def __init__(self):
        self.length = 16
        self.row = []
        self.start_index = 0
        # self.path = 'C:/Users/tobita/Desktop/test.bmp'
        # self.path = 'C:/Users/tobita/OneDrive/ドキュメント/cs/test.exe'
        self.path = './aaa.txt'
        self.output_path = 'C:/Users/tobita/OneDrive/デスクトップ/buf/test.txt'
    
    def set_path(self, p):
        self.path = p 
    
    def print_row(self, out_file):
        head = '{:04X} '.format(self.start_index)
        hexes =  ' '.join([str(n) for n in self.row])
        print('%s %s' %(head, hexes))
        out_file.write('%s %s\n' %(head, hexes))

    
    def analyse(self):
        byte_cnt = 0
        f = open(self.path, 'rb')
        with open(self.output_path, 'w') as out_file:
            while True:
                d = f.read(1)
                byte_cnt += 1
                if len(d) == 0:
                    self.print_row(out_file)
                    break
                else:
                    hex_int = int.from_bytes(d, 'little')
                    hex_str = '{:02X}'.format(hex_int)
                    self.row.append(hex_str)
                    if byte_cnt % self.length == 0:
                        self.print_row(out_file)
                        self.row.clear()
                        self.start_index += self.length   
            f.close()

if __name__ == '__main__':
    analyser = FileAnalyzer()
    analyser.analyse()