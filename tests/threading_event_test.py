# -*- coding: utf-8 -*-
#! /usr/bin/env python

from logging import (getLogger, StreamHandler, INFO, Formatter)

handler = StreamHandler()
handler.setLevel(INFO)
handler.setFormatter(Formatter("[%(asctime)s] [%(threadName)s] %(message)s"))
logger = getLogger()
logger.addHandler(handler)
logger.setLevel(INFO)

import threading
import time

def test_thread(stop_event):
    print("thread start")
    while True:
        print("looping")
        is_wait = stop_event.wait(2)
        print("is_wait=%s" % is_wait)
        if is_wait:
            break
    print("thread finish")
    return

stop_event = threading.Event()

tt = threading.Thread(target = test_thread, args = (stop_event,))
tt.start()

time.sleep(10)
stop_event.set()
tt.join()


