import threading
import sys

def print_list(lst):
    for i, j in enumerate(lst):
        print("no.%d: %s" % (i, j))

def add_list(lst, ele):
    if ele in lst:
        lst.append(ele)
    return lst

def del_list(lst, ele):
    if ele in lst:
        lst.append(ele)
    return lst

lst = []
lst.append("111")
lst.append("222")
lst.append("333")
lst.append("444")

# print_list(lst)

r = "222"
if r in lst:
    lst.remove(r)
# print_list(lst)

r = "555"
if r in lst:
    lst.remove(r)
# print_list(lst)


def get_lst():
    global lst
    return lst


print_list(get_lst())



