import urllib.request, urllib.error

url = 'http://python.org/'
headers = {}
headers['User-Agent'] = 'Googlebot'

req = urllib.request.Request(url, headers = headers)
with urllib.request.urlopen(req) as res:
    print(res.read())