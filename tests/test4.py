#! /usr/bin/env python

import socket
import threading
import sys
import argparse

is_loop = True

def get_is_loop():
    global is_loop
    return is_loop

def set_is_loop(bflag):
    global is_loop
    is_loop = bflag


def handle_client(client_socket):
    recv_size = 4096
    while get_is_loop():
        print("handle_client recv")
        r = client_socket.recv(recv_size)
        print("\n")
        dr = r.decode('utf-8')
        print("<<<"+ dr)
        if dr == "--bye" or dr == "--Bye":
            print("byebye")
            set_is_loop(False)
            client_socket.close()
            break
    
def handle_input():
    try:
        while get_is_loop():
            print("handle_input")
            i = input(">>>")
            if i=="--bye" or i=="--Bye":
                raise Exception
                set_is_loop(False)
                sys.exit(0)
                break
    # except EOFError:
    # except Exception:
    except:
        print("handle input Exception")
        set_is_loop(False)
        sys.exit()

def accept_socket(server):
    while get_is_loop():
        print("loop")
        client, addr = server.accept()
        print("[*] Accepted connection from: %s:%d" % (addr[0], addr[1]))
        client_handler = threading.Thread(target = handle_client, args = (client, ))
        client_handler.start()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", default = 8080,help="target port")
    args=parser.parse_args()
    max_socket_cnt = 5

    bind_ip = "localhost"
    bind_port = args.port
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind((bind_ip, bind_port))
    server.listen(max_socket_cnt)

    print("[*] Listening on %s:%d" % (bind_ip, bind_port))

    input_handler = threading.Thread(target = handle_input)
    input_handler.start()

    try:
        server.settimeout(3)
        # accept_socket(server)
        while get_is_loop():
            print("loop")
            client, addr = server.accept()
            print("[*] Accepted connection from: %s:%d" % (addr[0], addr[1]))
            client_handler = threading.Thread(target = handle_client, args = (client, ))
            client_handler.start()
    except KeyboardInterrupt:
        print("break loop")
        client.close()
        set_is_loop(False)
        sys.exit(1)

    print("THE END")
    
if __name__ == "__main__":
    main()