#! /usr/bin/env python

import subprocess

print("hello")
command = 'ls -la'

try:
    output = subprocess.check_output(
        command, stderr=subprocess.STDOUT, shell=True)
except subprocess.CalledProcessError as e:
    print(e.returncode)
    print(e.cmd)
    print(e.output)

print(output)