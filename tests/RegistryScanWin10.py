#参考URL
#https://www.mbsd.jp/blog/20190514.html

import os
import subprocess
import pathlib

path1 = 'HKLM\SAM\SAM\Domains\Account'
path2 = 'HKLM\SYSTEM\CurrentControlSet\Control\Lsa\Data'
subPath2=['JD', 'Skew1', 'GBG', 'Data']


#Phase1 
#レジストリ「HKLM\SAM\SAM\Domains\Account」path1のデータ取得

out = subprocess.run(['reg', 'query', path1], stdout = subprocess.PIPE)
outDecode = out.stdout.decode('cp932')
outDecodeSplit = outDecode.split()
for value in outDecodeSplit:
 print(value)

outSplitF=outDecode.split()[3]
dataF = outSplitF[0x88 : (0x97 + 1)]
print('dataF : ' + dataF)
iv = outSplitF[0x78 : (0x87 + 1)]
print('iv    : ' + iv)

outSplitV = outDecode.split()[6]
dataV = outSplitV[0x9c : (0x9f + 1)]
print('dataV : ' + dataV)

#path2のデータ取得
out = subprocess.run(['reg', 'query', path2], stdout = subprocess.PIPE)
outDecode = out.stdout.decode('cp932')
outSplit=outDecode.split()[3]
# outSplit=outDecode.split()
print(outDecode)

