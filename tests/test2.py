#! /usr/bin/env python

#boool = True
#digits = 3 if boool else 5

#print("digits:%d" % digits)

#s = "helloPython"

#ss = ' '.join([x for x in s])
#ss.join('\n')
#print(ss)

i =  chr(32)
#print("i:%s" % i)
o =ord("2")
print(o)
#print("%0*X" % (2, o))
#print("%0X" % o)

#j = 5
#if 1 <= j < 5: print("true")
#else: print("false")

min_hex = 0x20
max_hex = 0x7F
print("min_hex=%s, max_hex=%s" % (min_hex, max_hex))

for i in range(min_hex, max_hex):
    print("id=%03d, hex=%s, value=%s" % (i, hex(i), chr(i)))

data = '\n'
if not data:
    print("not")
else:
    print("yes")

#for i in range(0, 666, 16):
    #print("%04X" % i)

#text = 0x0001
#boolean = True if isinstance(text, str) else False
#print(boolean)