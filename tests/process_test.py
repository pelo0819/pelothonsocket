from multiprocessing import Process
from time import sleep

def func_1(num):
    print("start main process")
    for i in range(num):
        print("main process:", i)
        sleep(1)
    print("end main process")

def func_2(num):
    print("start sub process")
    for i in range(num):
        print("sub process:", i)
        sleep(0.5)
    print("end sub process")

if __name__ == '__main__':
    jobs =[]
    p = Process(target=func_2, args=(10,))
    jobs.append(p)
    p.start()
    p.join()
    func_1(10)