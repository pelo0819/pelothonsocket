using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace test
{
    class Sequencer
    {
        static void Main(string[] args)
        {
            Console.WriteLine("this is test");
            var house = new WareHouse();
            house.Init();
            house.Fire();
            Console.ReadKey();

        }
    }

    class WareHouse
    {
        public delegate void iFunction();
        public struct iArray
        {
            public iFunction iFunc;
        }

        public enum Type
        {
            Sword,
            Telephone,
            TV,
            Max
        }

        iArray[] funcs = new iArray[(int)Type.Max];

        public WareHouse()
        {
            Console.WriteLine("Const");
        }

        public void Init()
        {
            funcs[(int)Type.Sword].iFunc = Sword;
            funcs[(int)Type.Telephone].iFunc = Telephone;
            funcs[(int)Type.TV].iFunc = Telephone;
        }

        public void Fire()
        {
            funcs[(int)Type.Telephone].iFunc();
        }

        public void Sword()
        {
            Console.WriteLine("sword");
        }

        public void Telephone()
        {
            Console.WriteLine("telephone");
        }

        public void TV()
        {
            Console.WriteLine("TV");
        }

    }
}
