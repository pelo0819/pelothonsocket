#! /usr/bin/env python

import socket
import threading

socket_cnt=0
bind_ip = "192.168.3.7"
bind_port = 9999

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((bind_ip, bind_port))
max_socket_cnt = 5
server.listen(max_socket_cnt)

print("[*] Listening on %s:%d" % (bind_ip, bind_port))

def count_up_socket():
    global socket_cnt
    socket_cnt += 1

def handle_client(client_socket):
    recv_size = 4096
    request =""
    client_socket.settimeout(1)
    try:
        while True:
            r = client_socket.recv(recv_size)
            print("type recv: %s" % type(r))
            if not r:
                break
            request += r
    except:
        print("timeout. Go to next.")
    
    print("[*] Received: %s" % request)
    count_up_socket()
    msg = "your number is" + str(socket_cnt)
    msg = str.encode(msg, 'utf-8')
    #for i in range(1,10):
    #    msg += 'a'
    client_socket.send(msg)    
    client_socket.close()
    print("socket closed. GoodBye!!!")    
    
#    if socket_cnt >= max_socket_cnt:
#        client_socket.close()
#        print("socket closed. GoodBye!!!")

while True:
    client, addr = server.accept()
    print("[*] Accepted connection from: %s:%d" % (addr[0], addr[1]))
    client_handler = threading.Thread(target = handle_client, args = (client, ))
    client_handler.start()
    