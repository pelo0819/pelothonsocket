#! /usr/bin/env python

import socket
import threading

socket_cnt=0
bind_ip = "192.168.3.7"
bind_port = 9999
is_looping = True

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.close()
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((bind_ip, bind_port))
max_socket_cnt = 5
server.listen(max_socket_cnt)

print("[*] Listening on %s:%d" % (bind_ip, bind_port))

def count_up_socket():
    global socket_cnt
    socket_cnt += 1

def handle_client(client_socket):
    global is_looping
    recv_size = 4096
    request =""
    #client_socket.settimeout(1)
    try:
        while is_looping:
            request = client_socket.recv(recv_size)
            print("[*] Received: %s, type %s" % (request, type(request)))
            msg = "Received your message"
            msg = str.encode(msg, 'utf-8')
            client_socket.send(msg)
            if request == 'Bye' or request == 'bye':
                #is_looping = False
                break
    except:
        print("timeout. Go to next.")
    
    count_up_socket()
    msg = "socket closed. GoodBye!!!"
    msg = str.encode(msg, 'utf-8')
    
    client_socket.send(msg)    
    client_socket.close()    
    
while is_looping:
    client, addr = server.accept()
    print("[*] Accepted connection from: %s:%d" % (addr[0], addr[1]))
    client_handler = threading.Thread(
        target = handle_client, 
        args = (client, ))
    client_handler.start()
    