#! /usr/bin/env python

import socket
import threading

target_host = "192.168.3.7"
target_port = 9999
recv_size = 4096
is_looping = True
err_cnt = 0

def Bye():
    global is_looping
    is_looping = False


def host_recv_handler(soc):
    global is_looping
    print('\n')
    print("[*] host_recv_handler start.")
    while is_looping:
        r = soc.recv(recv_size)
        print('\n')
        recv_msg ="<<< " + str(r)
        print(recv_msg)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((target_host, target_port))

host_recv_thread = threading.Thread(
    target = host_recv_handler,
    args= (client,))
host_recv_thread.start()

while is_looping:
    try :
        z = input(">>> ")
        if "bye" == z or "Bye" == z:
            Bye()
            #client.close()
        if isinstance(z, str):
            msg = str.encode(z, 'utf-8')
            client.send(msg)
    except:
        print(">>> you shuold input enable words.")
        err_cnt += 1
        if err_cnt >= 10:
            Bye()
            client.close()

data, addr = client.recvfrom(4096)

print(data)