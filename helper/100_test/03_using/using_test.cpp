#include <stdio.h>

using test = int;

void my_print(test t);

int main(int argc, char **argv)
{
    printf("helloworld\n");
    my_print(6);
}

void my_print(test t)
{
    printf("%d", t);
}
