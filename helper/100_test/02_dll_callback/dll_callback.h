#pragma once

using p_callback_func = void(*)();

void __stdcall dll_callback(p_callback_func callback);