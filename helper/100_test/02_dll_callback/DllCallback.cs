using System;
using System.Runtime.InteropServices;


public class DllCallback
{
    static void Main(string[] args)
    {
        Console.WriteLine("this is test."); 
        Console.ReadKey();

        DllImportWrapper imp = new DllImportWrapper();
        imp.SetCallback();
        imp.callback_invoke();
    }
}

public class DllImportWrapper : DllImporter
{
    public DllImportWrapper(){}

    public void SetCallback()
    {
        set_callback(PurposeCallback);
    }

    private void PurposeCallback()
    {
        Console.WriteLine("[*] success to invoke callback.");
    }

}

public class DllImporter
{
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CallbackDelegate();

    [DllImport("dll/dll_callback.dll")]
    private extern static void dll_callback([MarshalAs(UnmanagedType.FunctionPtr)] CallbackDelegate callback);

    private CallbackDelegate action;

    public DllImporter()
    {
        Console.WriteLine("constructor");
    }

    public void callback_invoke()
    {
        dll_callback(action);
    }


    public void set_callback(CallbackDelegate callback)
    {
        action = callback;
    }
}