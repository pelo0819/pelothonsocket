#include <stdio.h>

#include "dll_callback.h"

void dll_callback(p_callback_func callback)
{
    callback();
    printf("[*] invoke callback.\n");
}