using System;
using System.Runtime.InteropServices;

public class DllTest
{
    static void Main(string[] args)
    {
        Console.WriteLine("this is test."); 
        DllImporter.print_name();
        Console.ReadKey();
    }
}

public class DllImporter
{
    [DllImport(".\\product\\dll_test.dll")]
    public extern static bool print_name();
}