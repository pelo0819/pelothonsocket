using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Xml.Linq;

public class MainRegMonitor
{
    static void Main(string[] args)
    {
        if(args.Length < 1)
        {
            Console.WriteLine("[!!!] input error.");
            return;
        }

        // var path = ".\\software\\Adobe\\Adobe Acrobat\\DC\\Security\\cASPKI\\cASPKI"
        // +"\\cCustomCertPrefs\\c290FA7E61053E8763C6055E6333A99EFB83ECACB\\";

        var path = args[0];
        try{
            Directory.CreateDirectory(".\\" + path);
            Console.WriteLine("[*] make directory to {0}", path);    
        }
        catch(IOException e)
        {
            Console.WriteLine("[!!!] MonitorRegRecurse Err: " + e.GetType().Name);
            Console.WriteLine("[!!!] Path Length: {0}", path.Length);
        }

        // var dirCur = Directory.GetCurrentDirectory();
        // Console.WriteLine("dirCur: " + dirCur);

        // Directory.SetCurrentDirectory(".");

        // dirCur = Directory.GetCurrentDirectory();
        // Console.WriteLine("change cur directory");
        // Console.WriteLine("dirCur after: " + dirCur);

        // try
        // {
        //     Dictionary<string, int> dict = new Dictionary<string, int>();
        //     dict.Add(args[0], 1);
        //     Console.WriteLine("[*] add dict: " + args[0]);
        //     if(dict.ContainsKey(args[0]))
        //     {
        //         Console.WriteLine("[*] contains key.");
        //     }
        //     else
        //     {
        //         Console.WriteLine("[*] does not contains key.");
        //     }
        // }
        // catch(IOException e)
        // {
        //     Console.WriteLine("[!!!] MonitorRegRecurse Err: " + e.GetType().Name);
        // }



        // Directory.CreateDirectory(path);
        // Console.WriteLine("[*] make directory to {0}", path);
    }
}
