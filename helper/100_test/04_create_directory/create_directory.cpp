#include <stdio.h>
#include <string.h>
#include <direct.h>

int main(int argc, char **argv)
{
    if(argc < 2)
    {
        printf("[!!!] input error.\n");
        return 1;
    }
    char *file_name = argv[1];
    int len = strlen(file_name);
    if(_mkdir(file_name) == 0)
    {
        printf("success make directory %s, len: %d\n", file_name, len);
    }
    else
    {
        printf("fail make directory %s, len: %d\n", file_name, len);
    }
    return 0;
}