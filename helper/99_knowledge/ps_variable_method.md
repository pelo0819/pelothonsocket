# PowerShellで知ったときへぇってなったこと
1. $testにdirコマンド結果を代入  
```$test= dir```  

1. $testの要素の取得  
```$test[0]```  
で一番最初の要素を取得、dirが配列じゃない場合は少ないからね  

1. $testの方を取得  
```$test[0].GetType().FullName```  

1. メソッドの実行  
↑で型を知れたら、Microsoft docsとかで調べるとメンバー変数、関数がすぐヒットする  
メソッドを実行したい時は、  
```$test[0]..GetDirectories()```  

## 参照
[その1](https://docs.microsoft.com/ja-jp/powershell/scripting/learn/deep-dives/everything-about-arrays?view=powershell-7.1)