using System;
using System.Reflection;

/// <summary>
/// シングルトンクラス用の親クラス
/// 継承クラスではsealed修飾子をつけること
/// 参考URL
/// https://smdn.jp/programming/dotnet-samplecodes/reflection/3a879650024111eb907175842ffbe222/
/// https://cloud6.net/so/c%23/3623055
/// </summary>
public class SingletonClass<T>
{
    private static T _instance;
    // private static object _syncRoot = new Object();

    protected SingletonClass(){}

    private static T CreateInstance()
    {
        return (T)typeof(T).GetConstructor(Type.EmptyTypes).Invoke(null);
    }

    public static T Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = CreateInstance();
            }
            return _instance;
        }
    }

}