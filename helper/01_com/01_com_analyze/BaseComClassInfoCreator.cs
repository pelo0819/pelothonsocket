using System;
using System.Diagnostics;
using System.IO;
using System.Management.Automation;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Win32;

public class BaseComClassInfoCreator
{
    /// <summary>
    /// 出力するか
    /// </summary>
    protected bool isOutput = false;

    /// <summary>
    /// 初期化したか
    /// </summary>
    protected bool isInitialized = false;

    /// <summary>
    /// Comクラスインフォのリスト
    /// </summary>
    /// <typeparam name="ComClassInfo"></typeparam>
    /// <returns></returns>
    public List<ComClassInfo> classInfos {get; private set;} = new List<ComClassInfo>();

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public BaseComClassInfoCreator()
    {
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public virtual void Init()
    {
        CreateComClassInfos();
        isInitialized = true;
    }

    /// <summary>
    /// classInfosを生成する
    /// </summary>
    public virtual void CreateComClassInfos(){}

    /// <summary>
    /// ComClassInfoを生成する
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public virtual ComClassInfo CreateComClassInfo(Object obj = null){ return null;}

    /// <summary>
    /// Comクラスインフォリストに追加
    /// </summary>
    /// <param name="info"></param>
    public void AddClassInfo(ComClassInfo info)
    {
        classInfos.Add(info);
    }

    /// <summary>
    /// 指定progIdからクラスインフォを取得
    /// </summary>
    /// <param name="progId"></param>
    /// <returns></returns>
    public ComClassInfo GetClassInfoProgId(string s)
    {
        return classInfos.FirstOrDefault(_ => _.progId == s);
    }

    /// <summary>
    /// 指定clsIdからクラスインフォを取得
    /// </summary>
    /// <param name="clsId"></param>
    /// <returns></returns>
    public ComClassInfo GetClassInfoClsId(string s)
    {
        return classInfos.FirstOrDefault(_ => _.clsId == s);
    }

    /// <summary>
    /// 指定文字列がprogIdに含まれるクラスインフォを追加
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public List<ComClassInfo> GetResultSearchProgId(string s)
    {
        var ret = new List<ComClassInfo>();
        var infos = classInfos.Where(_ => _.progId.ToLower().Contains(s.ToLower())).ToList();
        if(infos == null || infos.Count <= 0){return ret;}
        return infos;
    }

    /// <summary>
    /// 指定文字列がclsIdに含まれるクラスインフォを追加
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public List<ComClassInfo> GetResultSearchClsId(string s)
    {
        var ret = new List<ComClassInfo>();
        var infos = classInfos.Where(_ => _.clsId.ToLower().Contains(s.ToLower())).ToList();
        if(infos == null || infos.Count <= 0){return ret;}
        return infos;
    }

    /// <summary>
    /// ComClassInfoに内包されるmemInfosを作成する
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public virtual bool CreateComMemberInfos(string s){ return true; }

    public virtual ComMemberInfo CreateComMemberInfo(Object obj){ return null;}

    /// <summary>
    /// 指定文字列から前の文字列をトリムして返す
    /// </summary>
    /// <param name="str"></param>
    /// <param name="sep"></param>
    /// <returns></returns>
    public string pltrim(string str, string sep)
    {
        var idx = str.IndexOf(sep);
        if(idx >= 0){ return str.Substring(idx + sep.Length);}
        return str;
    }

    /// <summary>
    /// 指定文字列から後の文字列をトリムして返す
    /// </summary>
    /// <param name="str"></param>
    /// <param name="sep"></param>
    /// <returns></returns>
    public string prtrim(string str, string sep)
    {
        var idx = str.LastIndexOf(sep);
        if(idx >= 0){ return str.Substring(0, idx);}
        return str;
    }

    /// <summary>
    /// 出力をリセット
    /// </summary>
    public void ResetOutput()
    {
        var text = ""; 
        File.WriteAllText(@".\classinfo.pelo", text);
    }
}