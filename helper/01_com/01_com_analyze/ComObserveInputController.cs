using System;
using System.IO;

public class ComObserveInputController
{
    private ComClassInfoManager mgr = null;

    public ComObserveInputController(ComClassInfoManager mgr)
    {
        this.mgr = mgr;
    }

    /// <summary>
    /// モードを選択する
    /// </summary>
    /// <returns></returns>
    public bool InputSelectMode()
    {
        Console.WriteLine("[?] which load mode [p]:PowerShell, not[p]:text ?");
        var isOutputStr = Console.ReadLine(); 
        return isOutputStr.ToLower() == "p" ? true : false;
    }

    public void InputMain()
    {
        Console.WriteLine("[*] please input command.");
        while (true)
        {
            Console.Write(">>> ");
            var input = Console.ReadLine();
            if(input == "-help" || input == "-h")
            {
                Usage();
            }
            else if(input == "-l")
            {
                DispComProgIdList();
            }
            else if(input.Contains("-sp"))
            {
                DispSearchResultFromProgId(input);
            }
            else if(input.Contains("-sc"))
            {
                DispSearchResultFromClsId(input);
            }
            else if(input.Contains("-gp"))
            {
                DispGetResultFromProgId(input);
            }
            else if(input.Contains("-gc"))
            {
                DispGetResultFromClsId(input);   
            }
            else if(input.Contains("-r"))
            {
                InvokeMethod(input);
            }
            else if(input == "exit" || input == "bye")
            {
                Console.WriteLine("bye!!! please input any key.");
                ComOperator.Instance.End();
                break;
            }
        }
    }

    private void Usage()
    {
        Console.WriteLine(" [-help] or [-h] -> usage");
        Console.WriteLine(" [-l]            -> show List of all com object progId.");
        Console.WriteLine(" [-sp]           -> Search com object infos from ProgId.");
        Console.WriteLine(" [-sc]           -> Search com objcet infos from ClsId.");
        Console.WriteLine(" [-gp]           -> Get Members of com object from ProgId.");
        Console.WriteLine(" [-gc]           -> Get Members of com object from ClsId.");
        Console.WriteLine(" [-r] <progId> <method name>");
        Console.WriteLine(" [exit] or [bye] -> EXIT.");
    }

    private void DispComProgIdList()
    {
        Console.WriteLine("--- progId List ---");
        mgr.RunTimeCCICreator.classInfos?.ForEach(_ => 
        {
            Console.WriteLine("{0}", _.progId);
        });
    }

    private void DispSearchResultFromProgId(string input)
    {
        Console.WriteLine("[*] search!!!!!");
        var splits = input.Split(' ');
        if(splits.Length < 2){ShowInputErr();}
        else
        {
            var progId = splits[1];
            ShowSearchResultProgId(progId);
        }
    }

    private void DispSearchResultFromClsId(string input)
    {
        Console.WriteLine("[*] search!!!!!");
        var splits = input.Split(' ');
        if(splits.Length < 2){ShowInputErr();}
        else
        {
            var progId = splits[1];
            ShowSearchResultClsId(progId);
        }
    }

    private void DispGetResultFromProgId(string input)
    {
        var splits = input.Split(' ');
        if(splits.Length < 2){ShowInputErr();}
        else
        {
            var progId = splits[1];
            var classInfo = mgr.RunTimeCCICreator.GetClassInfoProgId(progId);
            if(classInfo == null){ShowInputErr();}
            else
            {
                if(mgr.RunTimeCCICreator.CreateComMemberInfos(progId))
                {
                    if(splits.Length == 3)
                    {
                        var option = splits[2].ToLower();
                        if(option == "-m")
                        {
                            classInfo.PrintMethodMembers();
                        }
                        else if(option == "-p")
                        {
                            classInfo.PrintPropertyMembers();
                        }
                    }
                    else if(splits.Length == 4)
                    {
                        var option = splits[2].ToLower();
                        var search = splits[3].ToLower();
                        if(option == "-m")
                        {
                            classInfo.PrintMethodMembers(search);
                        }
                        else if(option == "-p")
                        {
                            classInfo.PrintPropertyMembers(search);
                        }
                    }
                    else
                    {
                        classInfo.PrintAllMembers();
                    }
                }
                else{ShowInputErr();}
            }
        }
    }

    private void DispGetResultFromClsId(string input)
    {
        var splits = input.Split(' ');
        if(splits.Length < 2){ShowInputErr();}
        else
        {
            var clsId = splits[1];
            var classInfo = mgr.RunTimeCCICreator.GetClassInfoClsId(clsId);
            if(classInfo == null)
            {
                ShowInputErr();
            }
            else
            {
                if(mgr.RunTimeCCICreator.CreateComMemberInfos(classInfo.progId))
                {
                    classInfo.PrintAllMembers();
                }
                else{ShowInputErr();}
            }
        }
    }

    private void InvokeMethod(string input)
    {
        var splits = input.Split(' ');
        if(splits.Length < 3){ ShowInputErr();}
        var progId = splits[1];
        var methodName = splits[2];
        ComOperator.Instance.Invoke(progId, methodName);
    }

    private void ShowSearchResultProgId(string progId)
    {
        var results = mgr.RunTimeCCICreator.GetResultSearchProgId(progId);
        Console.WriteLine("--- search result progId ---");
        results?.ForEach(_ => _.Print());
    }

    private void ShowSearchResultClsId(string clsId)
    {
        var results = mgr.RunTimeCCICreator.GetResultSearchClsId(clsId);
        Console.WriteLine("--- search result clsId ---");
        results?.ForEach(_ => _.Print());
    }

    private void ShowInputErr()
    {
        Console.WriteLine("[!!!] input error. please retry or input -help.");
    }

}