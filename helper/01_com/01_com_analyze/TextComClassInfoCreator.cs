using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class TextComClassInfoCreator : BaseComClassInfoCreator
{
    public TextComClassInfoCreator(){}

    /// <summary>
    /// 初期化
    /// </summary>
    public override void Init()
    {
        LoadData();
    }

    /// <summary>
    /// classInfosを作成する
    /// </summary>
    public override void CreateComClassInfos()
    {

    }

    /// <summary>
    /// PSObjectからComClassInfoを生成する
    /// </summary>
    /// <param name="psObj"></param>
    /// <returns></returns>
    public override ComClassInfo CreateComClassInfo(Object obj = null)
    {
        var info = new ComClassInfo();
        return info;
    }

    private void LoadData()
    {
        var files = Directory.EnumerateFiles(@"..\99_data", "*.pelo").ToList();

        foreach (var f in files)
        {
            StreamReader r = new StreamReader(f);
            var progId = f;
            progId = pltrim(progId, "99_data\\");
            progId = prtrim(progId, ".pelo");
        
            var classInfo = CreateComClassInfo();
            classInfo.SetProgId(progId);
            AddClassInfo(classInfo);
        
            ReadLines(r, classInfo);
        
            r.Close();
        }
    }

    private void ReadLines(StreamReader r, ComClassInfo classInfo)
    {
        string line = "";
        var standBy = false;
        // 要素(Name, MemberType, Definition)を格納するリスト
        var items = new List<string>();
        while((line = r.ReadLine()) != null)
        {
            // ----が来るまでは何もしない
            if(line.StartsWith("----"))
            {
                // スタンバイ状態にする
                standBy = true;
            }

            if(standBy)
            {
                // "Name"から始まる行の場合
                if(!line.StartsWith("  "))
                {
                    items = new List<string>();
                    // 空欄で区切っただけの要素
                    var preItems = new List<string>();
                    // 空欄で区切った後、再形成した要素
                    var postItems = new List<string>();
                
                    // 空欄で文字列を区切る
                    var splits = line.Split(' ').ToList();
                    splits.ForEach(_ =>
                    {
                        // 意味を持つものだけ抽出
                        if(_ != ""){ preItems.Add(_); }
                    });
                
                    // Definitionでは、関数定義のところと引数のところで空欄があるので再形成する
                    //                               ↓ここのこと        ↓ここのこと
                    // System.Runtime.Remoting.ObjRef CreateObjRef(type requestedType)
                    foreach(var (item, idx) in preItems.Select((item, idx) => (item, idx)))
                    {
                        if(idx <= 2){ postItems.Add(item);}
                        else{ postItems[2] += " " + item; }
                    }

                    var memInfo = CreateComMemberInfo(postItems);
                    if(memInfo != null)
                    {
                        classInfo.AddComMemberInfo(memInfo);
                    }
                }
                else // 前の行が途中で終わって、Definitionの続きから始まる行の場合
                {
                    var memInfos = classInfo.memInfos;
                    var memInfo = memInfos[memInfos.Count() - 1];
                    var str = memInfo.definition;
                    memInfo.SetDefinition(str + line);
                }
                
            }
        }
    }

    /// <summary>
    /// progIdからmemInfosを生成する
    /// </summary>
    /// <param name="progId"></param>
    /// <returns></returns>
    public override bool CreateComMemberInfos(string progId)
    {
    
        return true;
    }

    /// <summary>
    /// ComMemberInfoを生成する
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override ComMemberInfo CreateComMemberInfo(Object obj)
    {
        var items = obj as List<string>;
        var info = new ComMemberInfo();

        if(items.Count() < 3){ return null; }
        info.SetName(items[0]);
        info.SetMemberType(items[1]);
        info.SetDefinition(items[2]);
        return info;
    }
}