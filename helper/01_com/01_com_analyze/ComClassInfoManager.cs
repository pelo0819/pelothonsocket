using System;
using System.Collections.Generic;

public class ComClassInfoManager
{
    public enum Mode
    {
        None,
        PowerShell,
        Text,
        Max
    }

    public Dictionary<Mode, BaseComClassInfoCreator> dictMode 
            = new Dictionary<Mode, BaseComClassInfoCreator>();
    
    private Mode mode = Mode.None;

    public BaseComClassInfoCreator RunTimeCCICreator => dictMode.ContainsKey(mode) ? dictMode[mode] : null; 

    public ComClassInfoManager()
    {
    }

    public void SwitchMode(Mode m)
    {
        mode = m;
        AddCCICreator(m);
    }

    /// <summary>
    /// dictModeに指定modeのCreatorを追加する
    /// </summary>
    /// <param name="m"></param>
    public void AddCCICreator(Mode m)
    {
        if(dictMode.ContainsKey(m)){ return; }

        BaseComClassInfoCreator ctr = null;
        switch (m)
        {
            case Mode.PowerShell:
                ctr = new PsComClassInfoCreator();
                break;
            case Mode.Text:
                ctr = new TextComClassInfoCreator();
                break;
            default:
                break;
        }

        if(ctr != null)
        {
            ctr.Init();
            dictMode.Add(m, ctr);
        }
    }

    // public BaseComClassInfoCreator GetRuntimeCCICreator()
    // {
    //     if(!dictMode.ContainsKey(mode)){ return null; }
    //     return dictMode[mode];
    // }

}