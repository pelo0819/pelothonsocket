using System;

public class ComMemberInfo
{
    public string name {get; private set;} = "";
    public string memberType{get;private set;} = "";
    public string definition{get; private set;} = "";
    // public string retType {get; private set;} = "";

    public ComMemberInfo(){}
    public void SetName(string s){name = s;}
    public void SetDefinition(string s){definition = s;}
    public void SetMemberType(string s){memberType = s;}
    // public void SetRetType(string s){retType = s;}
    public void Print()
    {
        Console.WriteLine(" -- member --");
        Console.WriteLine("     name      : {0}", name);
        Console.WriteLine("     memberType: {0}", memberType);
        Console.WriteLine("     definition: {0}", definition);
        Console.WriteLine("");
    }
}