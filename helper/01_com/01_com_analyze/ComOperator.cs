using System;
using System.Runtime.InteropServices;

public sealed class ComOperator : SingletonClass<ComOperator>
{
    string nameInstance = "";

    public void Initialize()
    {
        ComOpearationDll.Initialize();
    }

    public void CreateInstance(string progId)
    {
        // if(nameInstance == progId){ return; }
        ComOpearationDll.CreateInstance(progId);
        nameInstance = progId;
    }

    public void SetMethod(string name)
    {
        ComOpearationDll.SetMethod(name);
    }

    public void Invoke()
    {
        ComOpearationDll.Invoke();
    }

    public void Invoke(string name)
    {
        SetMethod(name);
        Invoke();
    }

    public void Invoke(string progId, string name)
    {
        CreateInstance(progId);
        SetMethod(name);
        Invoke();
    }

    public void End()
    {
        ComOpearationDll.End();
    }
}

public class ComOpearationDll
{
    [DllImport("ComDll.dll")]
    public extern static void Initialize();

    [DllImport("ComDll.dll")]
    public extern static void CreateInstance(string progId);

    [DllImport("ComDll.dll")]
    public extern static void SetMethod(string name);

    [DllImport("ComDll.dll")]
    public extern static void Invoke();

    [DllImport("ComDll.dll")]
    public extern static void End();
}