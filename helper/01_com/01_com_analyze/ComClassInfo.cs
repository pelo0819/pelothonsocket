using System;
using System.IO;
using System.Management.Automation;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;

public class ComClassInfo
{
    public string progId {get; private set;} = "";
    public string clsId {get; private set;} = "";
    public string valueOfClsId {get; private set;} = "";
    public List<ComMemberInfo> memInfos {get; private set;} = new List<ComMemberInfo>();
    public bool isSetMemberInfo {get; private set;} = false;

    public ComClassInfo(){}

    public void SetProgId(string s){ progId = s;}
    public void SetClsId(string s){ clsId = s;}
    public void SetValueOfClsId(string s){ valueOfClsId = s;}
    public void AddComMemberInfo(ComMemberInfo i){memInfos.Add(i);}
    public void SetFlagMemberSet(){isSetMemberInfo = true;}
    public void Print()
    {
        Console.WriteLine("-- COM class info");
        Console.WriteLine("progId:       {0}", progId);
        Console.WriteLine("clsId:        {0}", clsId);
        Console.WriteLine("valueOfClsId: {0}", valueOfClsId);
        Console.WriteLine("");
    }

    public void PrintAllMembers()
    {
        memInfos?.ForEach(_ => _.Print());
    }

    public void PrintMethodMembers(string s = null)
    {
        memInfos?.Where(_ => _.memberType.ToLower() == "method")?.ToList().ForEach(_ =>
        {
            if(s == null){ _.Print();}
            else
            {
                if(_.name.ToLower().Contains(s)){ _.Print();}
            }
        });
    }

    public void PrintPropertyMembers(string s = null)
    {
        memInfos?.Where(_ => _.memberType.ToLower() == "property")?.ToList().ForEach(_ =>
        {
            if(s == null){ _.Print();}
            else
            {
                if(_.name.ToLower().Contains(s)){ _.Print();}
            }
        });
    }

    public void WriteText()
    {
        var lstText = new List<string>();

        lstText.Add("" + progId + Environment.NewLine);

        lstText?.ForEach(_ =>
        {
            File.AppendAllText(@".\classinfo.pelo", _);
        });
    }
}