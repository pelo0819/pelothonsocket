using System;

public class ComSearcher
{
    static void Main(string[] args)
    {
        ComOperator.Instance.Initialize();
        var mgr = new ComClassInfoManager();
        mgr.SwitchMode(ComClassInfoManager.Mode.Text);
        var input = new ComObserveInputController(mgr);
        // input.InputSelectMode();
        input.InputMain(); 
        Console.ReadKey();
    }
}

