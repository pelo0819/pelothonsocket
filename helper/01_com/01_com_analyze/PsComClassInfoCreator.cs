using System;
using System.Diagnostics;
using System.IO;
using System.Management.Automation;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Win32;

public class PsComClassInfoCreator : BaseComClassInfoCreator
{
    public PsComClassInfoCreator()
    {
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public override void Init()
    {
        base.Init();
        if(isOutput)
        {
            ResetOutput();
        }
    }

    /// <summary>
    /// classInfosを作成する
    /// </summary>
    public override void CreateComClassInfos()
    {
        var classes = GetComClassInfoCollection()?.ToList();
        classes?.ForEach(c =>
        {
            var ci = CreateComClassInfo(c);    
            AddClassInfo(ci);
        });

        if(isOutput)
        {
            classInfos?.ForEach(i => i.WriteText());
        }
    }

    /// <summary>
    /// PowerShellコマンドを実行して、ClassInfo作成のための素材を取得する
    /// </summary>
    /// <returns></returns>
    private Collection<PSObject> GetComClassInfoCollection()
    {
        using(var invoker = new RunspaceInvoke())
        {
            return invoker.Invoke(@"dir REGISTRY::HKEY_CLASSES_ROOT\CLSID -Include PROGID -Recurse");
        }
    }

    /// <summary>
    /// PowerShellコマンドを実行して、ComMemberInfo作成のための素材を取得する
    /// </summary>
    /// <param name="progId"></param>
    /// <returns></returns>
    private Collection<PSObject> GetComMemberInfoCollection(string progId)
    {
        var command1 = "$o = New-Object -ComObject \"" + progId + "\"";
        var command2 = "Get-Member -InputObject $o";

        using(var invoker = new RunspaceInvoke())
        {
            try
            {
                Collection<PSObject> tmp = invoker.Invoke(command1);
                Collection<PSObject> ret = invoker.Invoke(command2);
                return ret;
            }
            catch(CmdletInvocationException e)
            {
                Console.WriteLine("[!!!] {0}", e);
                return new Collection<PSObject>();
            }
        }
    }

    /// <summary>
    /// PSObjectからComClassInfoを生成する
    /// </summary>
    /// <param name="psObj"></param>
    /// <returns></returns>
    public override ComClassInfo CreateComClassInfo(Object obj = null)
    {
        var psObj = obj as PSObject;
        var info = new ComClassInfo();
        info.SetProgId(GetProgIdString(psObj));
        var name = (psObj.Members["Name"].Value??"").ToString();
        info.SetClsId(GetClsIdString(name));
        info.SetValueOfClsId(GetValueOfClsIdString(name));
        return info;
    }

    /// <summary>
    /// powershellコマンドを実行してProgIdを取得する
    /// </summary>
    /// <param name="psObj"></param>
    /// <returns></returns>
    private string GetProgIdString(PSObject psObj)
    {
        object o = psObj.Methods["GetValue"].Invoke(new object[]{""});
        return (o??"").ToString();
    }

    /// <summary>
    /// PowerShell実行結果の文字列からclsIdを取得する
    /// {}付いたままだけど
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private string GetClsIdString(string name)
    {
        string[] split = name.Split('\\');
        if(split.Length < 2){return "";}
        return split[split.Length - 2];
    }

    /// <summary>
    /// PowerShell実行結果の文字列からvalueOfClsIdを取得する
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private string GetValueOfClsIdString(string name)
    {
        var ret = "";
        // レジストリにアクセスできるパス
        var path = name;
        var sep = "\\";
        path = pltrim(path, sep);
        path = prtrim(path, sep);

        if(path == null || path == ""){return "";}

        RegistryKey key = Registry.ClassesRoot.OpenSubKey(path);
        if(key == null){return "";}

        ret = (key.GetValue("")??"").ToString();
        key.Close();
        return ret;
    }

    /// <summary>
    /// progIdからmemInfosを生成する
    /// </summary>
    /// <param name="progId"></param>
    /// <returns></returns>
    public override bool CreateComMemberInfos(string progId)
    {
        // progIdからComClassInfoを取得
        var classInfo = GetClassInfoProgId(progId);
        if(classInfo == null){ return false;}

        // すでにmemInfosを生成済みなら何もしない
        if(classInfo.isSetMemberInfo){ return true;}

        // PowerShellコマンドを実行して結果を取得
        // 結果は、ComMemberInfoの材料
        var psObjs = GetComMemberInfoCollection(progId).ToList();
        if(psObjs == null || psObjs.Count <= 0){ return false;}

        psObjs.ForEach(o => 
        {
            var memInfo = CreateComMemberInfo(o);
            classInfo.AddComMemberInfo(memInfo);
        });

        // 生成済みフラグを立てる
        classInfo.SetFlagMemberSet();

        return true;
    }

    /// <summary>
    /// ComMemberInfoを生成する
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override ComMemberInfo CreateComMemberInfo(Object obj)
    {
        var psObj = obj as PSObject;
        var info = new ComMemberInfo();
        info.SetName(GetNameString(psObj));
        info.SetMemberType(GetMemberTypeString(psObj));
        info.SetDefinition(GetDefinitionString(psObj));
        return info;
    }

    /// <summary>
    /// メンバ名を取得する
    /// </summary>
    /// <param name="psObj"></param>
    /// <returns></returns>
    private string GetNameString(PSObject psObj)
    {
        return (psObj.Members["Name"].Value??"").ToString();
    }

    /// <summary>
    /// 変数か、関数か、イベントか
    /// </summary>
    /// <param name="psObj"></param>
    /// <returns></returns>
    private string GetMemberTypeString(PSObject psObj)
    {
        return (psObj.Members["MemberType"].Value??"").ToString();
    }

    /// <summary>
    /// 定義を取得
    /// </summary>
    /// <param name="psObj"></param>
    /// <returns></returns>
    private string GetDefinitionString(PSObject psObj)
    {
        return (psObj.Members["Definition"].Value??"").ToString();
    }

}