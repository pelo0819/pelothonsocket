# $o = New-Object -ComObject "ExcelAddIn.Connect.14"
# Get-Member -InputObject $o > res.txt

$path = Split-Path -Parent $MyInvocation.MyCommand.Path
Set-Location $path

$fileName = $path + ".\lst_com_objects.pelo"
$file = New-Object System.IO.StreamReader($fileName, [System.Text.Encoding]::GetEncoding("sjis"))
while(($line = $file.ReadLine()) -ne $null)
{
    $o = New-Object -ComObject $line
    $f = ".\data\" + $line + ".pelo"
    Get-Member -InputObject $o | Format-Table -AutoSize -Wrap > $f
    $donelist = ".\done.pelo"
    echo $line >> $donelist
}


$file.Close()
write-host("the end.")