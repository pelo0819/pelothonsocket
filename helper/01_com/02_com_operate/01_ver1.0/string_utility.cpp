#include "string_utility.h"

wchar_t *StringUtility::charToW(char *str)
{
    wchar_t* ret = new wchar_t[MAX_LEN];
    mbstowcs(ret, str, MAX_LEN);    
    return ret;
}

char *StringUtility::strToChar(std::string str)
{
    char* ret = new char[str.size() + 1];
    std::strcpy(ret, str.c_str());
    return ret; 
}

int StringUtility::get_digit(int num)
{
    int digit = 0;
    while(num != 0)
    {
        num /= 10;
        digit++;
    }
    return digit;
}

std::vector<std::string> StringUtility::split(std::string str, char pattern) {
    int first = 0;
    int last = str.find_first_of(pattern);

    std::vector<std::string> ret;

    while (first < str.size()) {
        std::string subStr(str, first, last - first);

        ret.push_back(subStr);

        first = last + 1;
        last = str.find_first_of(pattern, first);

        if (last == std::string::npos) {
            last = str.size();
        }
    }

    return ret;
}

