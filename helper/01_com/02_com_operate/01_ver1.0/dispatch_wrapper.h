#include <wbemidl.h>
#include <stdio.h>

class DispatchWrapper
{
    private:
    IDispatch *m_disp;
    public:
    DispatchWrapper();
    ~DispatchWrapper();
    void set_disp(IDispatch *d);
    IDispatch *get_disp();
};
