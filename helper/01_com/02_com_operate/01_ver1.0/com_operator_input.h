#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <wbemidl.h>
#include <map>
#include <string>

#include "dispatch_wrapper.h"

#pragma comment(lib, "Ole32.lib")
#pragma comment(lib, "oleaut32.lib")

#define MAX_LEN 256

class ComOperator
{
    private:
    std::map<std::string,DispatchWrapper> dict;

    public:
    ComOperator();
    ~ComOperator();
    bool init();
    bool set_app(const char *pprogid);
    bool set_method(const char *name, bool ismethod...);
    bool invoke(const char *disp_name, const char *method_name, int type, char *arg = "null");

    bool invoke_internal(
        IDispatch *pdisp,
        const char *name,
        int type,
        VARIANT *pret,
        int argc...);
    
    bool end();

    void add_dict(const char *name, IDispatch *d);
    IDispatch* get_dict_val_str(const std::string name);
    IDispatch* get_dict_val(const char *name);
};