#include "com_operator_input.h"
#include "variant_utility.h"
#include "string_utility.h"

ComOperator::ComOperator(){}

ComOperator::~ComOperator(){}

bool ComOperator::init()
{
    HRESULT hr;
    // COMを利用できるようにする
    hr = CoInitializeEx(0, COINIT_MULTITHREADED);
    if(FAILED(hr))
    {
        printf("[!!!] failed CoInitializeEx()\n");
        return 1;
    }
    printf("[*] success CoInitializeEx()\n");
    return 0;
}

bool ComOperator::set_app(const char *pprogid)
{
    wchar_t* c = new wchar_t[MAX_LEN];
    mbstowcs(c, pprogid, MAX_LEN);
    // OLECHAR* szMember = c;
    LPOLESTR progid = c;
    CLSID clsid;
    HRESULT hr = CLSIDFromProgIDEx(progid, &clsid);
    if(FAILED(hr))
    {
        printf("[!!!] failed CLSIDFromProgIDEx()\n");
        return false;
    }
    printf("[*] success CLSIDFromProgIDEx()\n");
    IDispatch *pdisp = (IDispatch *)calloc(1, sizeof(IDispatch));
    hr = CoCreateInstance(
            clsid, 
            0, 
            CLSCTX_LOCAL_SERVER, 
            IID_IDispatch, 
            reinterpret_cast<LPVOID *>(&pdisp));

    if(FAILED(hr))
    {
        printf("[!!!] failed CoCreateInstance()\n");
        return false;
    }

    printf("[*] success CoCreateInstance()\n");
    add_dict(pprogid, pdisp);
    return true;
}

bool ComOperator::set_method(const char *name, bool ismethod...)
{
    return true;
}

bool ComOperator::invoke(const char *disp_name, const char *method_name, int type, char *arg)
{
    int argc = 1;
    VARIANT ret;
    VariantInit(&ret);
    
    VARIANT x;
    VariantInit(&x);
    if(strcmp(arg, "null") != 0)
    {
        x = VariantUtility::get_variant(arg);
    }
    else
    {
        argc = 0;
        x.vt = VT_NULL;
    }

    invoke_internal(get_dict_val(disp_name), method_name, type, &ret, argc, x);
    add_dict(method_name, ret.pdispVal);

    return true;
}

bool ComOperator::invoke_internal(
    IDispatch *pdisp,
    const char *name,
    int type,
    VARIANT *pret,
    int argc...)
{
    if(!pdisp){
        printf("[!!!] invoker doesn't exist, so [%s] couldn't invoke.\n", name);
        return false;
    }

    // convert char to OLECHAR
    wchar_t* c = new wchar_t[MAX_LEN];
    mbstowcs(c, name, MAX_LEN);
    OLECHAR* szMember = c;
    HRESULT hr;
    DISPID dispid;
    hr = pdisp->GetIDsOfNames(
        IID_NULL,            // may be always IDD_NULL
        &szMember,           // method or property name
        1,                   // dispid count
        LOCALE_USER_DEFAULT, // may be LOCALE_USER_DEFAULT
        &dispid);            // method or property dispid

    if(FAILED(hr))
    {
        printf("[!!!] failed to GetIDsOfNames %s\n", name);
        return false;
    }
    printf("[*] success to GetIDsOfNames %s\n", name);

    // variadic argument
    va_list argv;
    va_start(argv, argc);   
    VARIANT *pargs = (VARIANT *)calloc(argc, sizeof(VARIANT));
    for(int i = 0; i < argc; i++)
    {
        pargs[i] = va_arg(argv, VARIANT);
    }
    va_end(argv);

    DISPPARAMS dp = {NULL, NULL, 0, 0};
    dp.cArgs = argc;
    dp.rgvarg = pargs;
    if(type & DISPATCH_PROPERTYPUT)
    {
        DISPID t = DISPID_PROPERTYPUT;
        dp.cNamedArgs = 1;
        dp.rgdispidNamedArgs = &t;
    }

    hr = pdisp->Invoke(
        dispid,              // method or property name
        IID_NULL,            // may be always IDD_NULL
        LOCALE_USER_DEFAULT, // i dont know
        type,                // method(DISPATCH_METHOD) or property(DISPID_PROPERTYPUT)
        &dp,                 // args
        pret,                // return
        NULL,                // i dont know , ignore
        NULL);               // i dont know , ignore
    
    if(FAILED(hr))
    {
        printf("[!!!] failed to Invoke %s\n", name);
        return false;
    }
    printf("[*] success to Invoke %s\n", name);

    delete [] pargs;

    return true;
}

bool ComOperator::end()
{
    CoUninitialize();
    printf("[*] end\n");
    return true;
}

void ComOperator::add_dict(const char *name, IDispatch *d)
{
    if(d != NULL)
    {
        DispatchWrapper value;
        value.set_disp(d);
        std::string key = name;
        auto ret = dict.insert(std::make_pair(key, value));
        if(ret.second)
        {
            printf("   --> add new disp %s\n", name);
        }
        else
        {
            printf("   --> %s already exists.\n", name);
        }
    }
}

IDispatch *ComOperator::get_dict_val_str(const std::string name)
{
    auto itr = dict.find(name);
    if(itr != dict.end())
    {
        return dict[name].get_disp();        
    }
    return NULL;
}

IDispatch *ComOperator::get_dict_val(const char *name)
{
    std::string key = name;
    return get_dict_val_str(name);
}

