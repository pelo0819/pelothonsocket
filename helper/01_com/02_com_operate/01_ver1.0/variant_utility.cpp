#include "variant_utility.h"
#include "string_utility.h"

VARIANT VariantUtility::get_variant(char *str)
{
    VARIANT ret;
    std::string s = str;
    int pos = s.find('-');
    if(pos == 0)
    {
        str++;
        std::string s = str;
        if(isdigit(*str)){ return get_variant_digit(str); }
        else{ return get_variant_str(str); }
    }
    else //str
    {
        return get_variant_str(str);
    }
}

VARIANT VariantUtility::get_variant_digit(char *str)
{
    VARIANT ret;
    std::string s = str;
    int integer = strtol(str, &str, 10) * -1;
    int pos = s.find('.');
    if(pos == -1)
    {
        ret.vt = VT_I4;
        ret.lVal = integer;
        return ret;
    }
    else
    {
        str++;
        int decimal_int = 0;
        double decimal_double = 0;
        int digit = 0;
        if(isdigit(*str))
        {
            decimal_int = strtol(str, &str, 10);
            decimal_double = decimal_int;
            digit = StringUtility::get_digit(decimal_int);
        }
        decimal_double = decimal_int;
        ret.vt = VT_R8;
        ret.dblVal = integer - (double)(decimal_double / pow(10, digit));
        return ret;
    }
}

VARIANT VariantUtility::get_variant_str(char *str)
{
    VARIANT ret;
    wchar_t* buf = StringUtility::charToW(str);
    ret.vt = VT_BSTR;
    ret.bstrVal = SysAllocString(buf);
    return ret;
}

void VariantUtility::print_variant(VARIANT v)
{
    if(v.vt == VT_BSTR) // string
    {
        std::wcout << "bstrVal: " << v.bstrVal << std::endl;
    }
    else if(v.vt == VT_I4) // int
    {
        std::cout << "lVal: " << v.lVal << std::endl;
    }
    else if(v.vt == VT_R8)// double
    {
        std::cout << "dblVal: " << v.dblVal << std::endl;
    }    
}
