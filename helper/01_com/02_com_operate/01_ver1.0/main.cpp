// 参考URL
// https://docs.microsoft.com/ja-jp/previous-versions/office/troubleshoot/office-developer/automate-excel-from-c
// http://eternalwindows.jp/com/auto/auto01.html
// https://maggothand.at.webry.info/200903/article_2.html
// https://ichigopack.net/win32com/com_automation_4.html

#include <vector>
#include <string>
#include <iostream>
#include <cstring>

#include "com_operator_input.h"
#include "variant_utility.h"
#include "string_utility.h"

void input_loop(ComOperator *com);
bool input_base(char *ret);
bool input_set_app(ComOperator *com, std::string str);
bool input_set_app_init(ComOperator *com, std::string str);
bool input_invoke(ComOperator *com, std::string str);
void print_usage_invoke();
void print_usage_set_app();

bool is_set_app = false;

int main()
{
#if false
    ComOperator *com = new ComOperator;
    com->init();
    char *name_app = "Excel.Application";
    com->set_app(name_app); 
    com->invoke("Excel.Application", "Visible", DISPATCH_PROPERTYPUT, "1");
    com->invoke("Excel.Application", "Workbooks", DISPATCH_PROPERTYGET);
    com->invoke("Workbooks", "Add", DISPATCH_PROPERTYGET);
    com->invoke("Excel.Application", "ActiveSheet", DISPATCH_PROPERTYGET);
    com->invoke("ActiveSheet", "Range", DISPATCH_PROPERTYGET, "A1");
    com->invoke("Rage", "Value", DISPATCH_PROPERTYPUT, "66");
    com->invoke("Add", "Saved", DISPATCH_PROPERTYPUT, "1");
    com->invoke("Excel.Application", "Quit", DISPATCH_METHOD);
    // com->invoke(pbook, "SaveAs", DISPATCH_METHOD, NULL, 0);
    com->end();
    delete com;
#else
    ComOperator *com = new ComOperator;
    com->init();
    input_loop(com);
    com->end();
    delete com;
#endif
    return 0;
}

void input_loop(ComOperator *com)
{
    for(;;)
    {
        if(!is_set_app)
        {
            printf("please input app name.\n");        
        }

        char *in = (char *)calloc(MAX_LEN, sizeof(char));
        if(!input_base(in)){ break;}
        
        if(strcmp(in, "-end") == 0){ break; }

        std::string str = in;

        if(input_set_app(com, str)){ continue; }

        if(!input_invoke(com, str)){ continue; }

        free(in);
    }
}

bool input_base(char *ret)
{
    char in[MAX_LEN];
    size_t len;
    printf(">>> ");
    if(fgets(in, MAX_LEN, stdin) == NULL){return false;}
    len = strlen(in);
    in[len - 1] = '\0';
    strcpy(ret, in);
    return true;
}

bool input_set_app(ComOperator *com, std::string str)
{
    if(input_set_app_init(com, str)){ return true;}
    std::vector<std::string> splits = StringUtility::split(str, ' '); 
    
    if(splits[0] == "-set")
    {
        if(splits.size() < 2)
        { 
            print_usage_set_app();
            return true;
        }
        char *name_app = StringUtility::strToChar(splits[1]);
        com->set_app(name_app);
        return true;
    }

    return false;   
}

bool input_set_app_init(ComOperator *com, std::string str)
{
    if(is_set_app){return false;}

    std::vector<std::string> splits = StringUtility::split(str, ' ');
    char *name_app = StringUtility::strToChar(splits[0]);
    if(!com->set_app(name_app)){ return true;}
    is_set_app = true;
    print_usage_invoke();
    return true;
}

bool input_invoke(ComOperator *com, std::string str)
{
    std::vector<std::string> splits = StringUtility::split(str, ' '); 

    // invoker method type arg
    if(splits.size() < 3)
    { 
        print_usage_invoke();
        return false;
    }
        
    char *invoker = StringUtility::strToChar(splits.at(0));
    char *method = StringUtility::strToChar(splits.at(1));
    int type = DISPATCH_PROPERTYGET;
    char *type_str = StringUtility::strToChar(splits.at(2));
    char *arg = "null";

    if(isdigit(*type_str))
    {
        type = strtol(type_str, &type_str, 10);
        switch (type)
        {
        case 0:
            type = DISPATCH_METHOD;
            break;
        case 1:
            type = DISPATCH_PROPERTYPUT;
            break;
        default:
            type = DISPATCH_PROPERTYGET;
            break;
        }
    }

    if(splits.size() >= 4)
    {
        char *arg_buf = StringUtility::strToChar(splits.at(3));
        arg = arg_buf;
    }

    printf("    invoker: %s\n", invoker);
    printf("    method : %s\n", method);
    switch(type)
    {
        case DISPATCH_METHOD:
            printf("    type   : DISPATCH_METHOD\n");
            break;
        case DISPATCH_PROPERTYPUT:
            printf("    type   : DISPATCH_PROPERTYPUT\n");
            break;
        default:
            printf("    type   : DISPATCH_PROPERTYGET\n");
            break;
       }
    printf("    arg    : %s\n", arg);

    com->invoke(invoker, method, type, arg);

    return true;
}

void print_usage_invoke()
{
    printf("--- usage of invoke ---\n");
    printf("    usage: [invoker] [method] [type] [arg]\n");
    printf("    [type]: 0      -> DISPATCH_METHOD,\n");
    printf("            1      -> DISPATCH_PROPERTYPUT,\n");
    printf("            others -> DISPATCH_PROPERTYGET,\n");
}

void print_usage_set_app()
{
    printf("--- usage of set app ---\n");
    printf("    usage: -set [invoker]\n");
}