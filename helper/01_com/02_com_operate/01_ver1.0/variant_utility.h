#include <ctype.h>
#include <stdlib.h>
#include <windows.h>
#include <math.h>
#include <string>
#include <iostream>

#pragma comment(lib, "Ole32.lib")
#pragma comment(lib, "oleaut32.lib")

#define MAX_LEN 256

class VariantUtility
{
public:
    static VARIANT get_variant(char *str);
    static void print_variant(VARIANT v);
private:
    static VARIANT get_variant_digit(char *str);
    static VARIANT get_variant_str(char *str);
};