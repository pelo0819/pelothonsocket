#include "dispatch_wrapper.h"

DispatchWrapper::DispatchWrapper(){}
DispatchWrapper::~DispatchWrapper(){}

void DispatchWrapper::set_disp(IDispatch *d)
{
    m_disp = d;
}

IDispatch *DispatchWrapper::get_disp()
{
    return m_disp;
}
