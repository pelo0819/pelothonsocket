#include <vector>
#include <string>

#define MAX_LEN 256

class StringUtility
{
public:
    static wchar_t *charToW(char *str);
    static char *strToChar(std::string str);
    static int get_digit(int num);
    static std::vector<std::string> split(std::string str, char patter);
private:
};