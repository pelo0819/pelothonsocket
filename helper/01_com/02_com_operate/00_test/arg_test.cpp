#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <windows.h>
#include <math.h>

#include <string>
#include <iostream>

#pragma comment(lib, "Ole32.lib")
#pragma comment(lib, "oleaut32.lib")

#define MAX_LEN 256

typedef enum
{
    T_NONE,
    T_STRING,
    T_INT,
    T_DOUBLE,
} Type;

VARIANT get_variant(char *str);
VARIANT get_variant_digit(char *str);
VARIANT get_variant_str(char *str);
int get_digit(int num);
wchar_t *CharToW(char *str);
void print_variant(VARIANT v);

Type type = T_NONE;

int main(int argc, char** argv)
{
    char *s = argv[1];
    VARIANT v = get_variant(s);
    print_variant(v);
    return 0;
}

VARIANT get_variant(char *str)
{
    VARIANT ret;
    std::string s = str;
    int pos = s.find('-');
    if(pos == 0)
    {
        str++;
        std::string s = str;
        if(isdigit(*str)){ return get_variant_digit(str); }
        else{ return get_variant_str(str); }
    }
    else //str
    {
        return get_variant_str(str);
    }
}

VARIANT get_variant_digit(char *str)
{
    VARIANT ret;
    std::string s = str;
    int integer = strtol(str, &str, 10) * -1;
    int pos = s.find('.');
    if(pos == -1)
    {
        ret.vt = VT_I4;
        ret.lVal = integer;
        type = T_INT;
        return ret;
    }
    else
    {
        str++;
        int decimal_int = 0;
        double decimal_double = 0;
        int digit = 0;
        if(isdigit(*str))
        {
            decimal_int = strtol(str, &str, 10);
            decimal_double = decimal_int;
            digit = get_digit(decimal_int);
        }
        decimal_double = decimal_int;
        ret.vt = VT_R8;
        ret.dblVal = integer - (double)(decimal_double / pow(10, digit));
        type = T_DOUBLE;
        return ret;
    }
}

VARIANT get_variant_str(char *str)
{
    VARIANT ret;
    wchar_t* buf = CharToW(str);
    ret.vt = VT_BSTR;
    ret.bstrVal = SysAllocString(buf);
    type = T_STRING;
    return ret;
}

int get_digit(int num)
{
    int digit = 0;
    while(num != 0)
    {
        num /= 10;
        digit++;
    }
    return digit;
}

wchar_t *CharToW(char *str)
{
    wchar_t* ret = new wchar_t[MAX_LEN];
    mbstowcs(ret, str, MAX_LEN);    
    return ret;
}

void print_variant(VARIANT v)
{
    if(v.vt == VT_BSTR)
    {
        std::wcout << "bstrVal: " << v.bstrVal << std::endl;
    }
    else if(v.vt == VT_I4)
    {
        std::cout << "lVal: " << v.lVal << std::endl;
    }
    else if(v.vt == VT_R8)
    {
        std::cout << "dblVal: " << v.dblVal << std::endl;
    }    
}
