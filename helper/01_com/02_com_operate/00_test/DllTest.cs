using System;
using System.Runtime.InteropServices;

public class DllTest
{
    static void Main(string[] args)
    {

        MyDll.Test("hello");
        MyDll.Initialize();
        MyDll.CreateInstance("Excel.ChartApplication.16");
        MyDll.SetMethod("Help");
        MyDll.Invoke();
        MyDll.End();
    }
}

public class MyDll
{
    [DllImport("ComDll.dll")]
    public extern static void Initialize();

    [DllImport("ComDll.dll")]
    public extern static void CreateInstance(string progId);

    [DllImport("ComDll.dll")]
    public extern static void SetMethod(string name);

    [DllImport("ComDll.dll")]
    public extern static void Invoke();

    [DllImport("ComDll.dll")]
    public extern static void End();
}