
// 参考URL
// https://docs.microsoft.com/ja-jp/previous-versions/office/troubleshoot/office-developer/automate-excel-from-c
// http://eternalwindows.jp/com/auto/auto01.html
// https://maggothand.at.webry.info/200903/article_2.html
// https://ichigopack.net/win32com/com_automation_4.html

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <oleauto.h>
#include <wbemidl.h>

#pragma comment(lib, "Ole32.lib")
#pragma comment(lib, "oleaut32.lib")
#define MAX_LEN 256

bool invoke(
    IDispatch *pdisp,
    const char *name,
    int type,
    VARIANT *pret,
    int args...);

int main()
{
    HRESULT hr;
    // COMを利用できるようにする
    hr = CoInitializeEx(0, COINIT_MULTITHREADED);
    if(FAILED(hr))
    {
        printf("[!!!] failed CoInitializeEx()\n");
        return 1;
    }
    printf("[*] success CoInitializeEx()\n");

    CLSID clsId;
    LPOLESTR progId = L"Excel.Application";
    hr = CLSIDFromProgIDEx(progId, &clsId);
    if(FAILED(hr))
    {
        printf("[!!!] failed CLSIDFromProgIDEx()\n");
        return 1;
    }
    printf("[*] success CLSIDFromProgIDEx()\n");

    IDispatch* papp = NULL;
    hr = CoCreateInstance(
            clsId, 
            0, 
            CLSCTX_LOCAL_SERVER, 
            IID_IDispatch, 
            reinterpret_cast<LPVOID *>(&papp));

    if(FAILED(hr))
    {
        printf("[!!!] failed CoCreateInstance()\n");
        return 1;
    }
    printf("[*] success CoCreateInstance()\n");
    
    VARIANT x;
    x.vt = VT_I4;
    x.lVal = 1;
    invoke(papp, "Visible", DISPATCH_PROPERTYPUT, NULL, 1, x);

    VARIANT ret;
    VariantInit(&ret);
    invoke(papp, "Workbooks", DISPATCH_PROPERTYGET, &ret, 0);
    IDispatch *pbooks;
    pbooks = ret.pdispVal;

    IDispatch *pbook;
    VariantInit(&ret);
    invoke(pbooks, "Add", DISPATCH_PROPERTYGET, &ret, 0);
    pbook = ret.pdispVal;

    VARIANT array;
    array.vt = VT_ARRAY | VT_VARIANT;
    SAFEARRAYBOUND sab[2];
    sab[0].lLbound = 1;
    sab[0].cElements = 15;
    sab[1].lLbound = 1;
    sab[1].cElements = 15;
    array.parray = SafeArrayCreate(VT_VARIANT, 2, sab);

    for(int i=1; i<=15; i++) {
      for(int j=1; j<=15; j++) {
         // Create entry value for (i,j)
         VARIANT tmp;
         tmp.vt = VT_I4;
         tmp.lVal = i*j;
         // Add to safearray...
         long indices[] = {i,j};
         SafeArrayPutElement(array.parray, indices, (void *)&tmp);
      }
    }

    IDispatch *psheet;
    VariantInit(&ret);
    invoke(papp, "ActiveSheet", DISPATCH_PROPERTYGET, &ret, 0);
    psheet = ret.pdispVal;

    IDispatch *prange;
    VARIANT parm;
    parm.vt = VT_BSTR;
    // parm.bstrVal = SysAllocString(L"A1:O15");
    parm.bstrVal = SysAllocString(L"A1");
    VariantInit(&ret);
    invoke(psheet, "Range", DISPATCH_PROPERTYGET, &ret, 1, parm);
    VariantClear(&parm);
    prange = ret.pdispVal;

    VariantInit(&array);
    array.vt = VT_I4;
    array.lVal = 6;
    invoke(prange, "Value", DISPATCH_PROPERTYPUT, NULL, 1, array);
    invoke(pbook, "Saved", DISPATCH_PROPERTYPUT, NULL, 1, x);

    invoke(pbook, "SaveAs", DISPATCH_METHOD, NULL, 0);
    // invoke(papp, "Quit", DISPATCH_METHOD, NULL, 0);


    // invoke(papp, "Help", DISPATCH_METHOD, NULL, 0);

    CoUninitialize();
    printf("[*] end\n");

#if false
    char buffer[MAX_LEN];
    wchar_t* c = new wchar_t[MAX_LEN];
    printf("Input method name ...\n");
    printf(">>>");
    if(fgets(buffer, MAX_LEN, stdin) == NULL)
    {
        return 1;
    }
    buffer[strlen(buffer) - 1] = '\0';
    mbstowcs(c, buffer, MAX_LEN);
    // method or property name
    OLECHAR* szMember = c;
#endif
}

bool invoke(
    IDispatch *pdisp,
    const char *name,
    int type,
    VARIANT *pret,
    int argc...)
{
    if(!pdisp){return false;}

    // convert char to OLECHAR
    wchar_t* c = new wchar_t[MAX_LEN];
    mbstowcs(c, name, MAX_LEN);
    OLECHAR* szMember = c;

    HRESULT hr;
    DISPID dispid;
    hr = pdisp->GetIDsOfNames(
        IID_NULL,            // may be always IDD_NULL
        &szMember,           // method or property name
        1,                   // dispid count
        LOCALE_USER_DEFAULT, // may be LOCALE_USER_DEFAULT
        &dispid);            // method or property dispid

    if(FAILED(hr))
    {
        printf("[!!!] failed to GetIDsOfNames %s\n", name);
        return false;
    }
    printf("[*] success to GetIDsOfNames %s\n", name);

    // variadic argument
    va_list argv;
    va_start(argv, argc);   
    VARIANT *pargs = (VARIANT *)calloc(argc, sizeof(VARIANT));
    for(int i = 0; i < argc; i++)
    {
        pargs[i] = va_arg(argv, VARIANT);
    }
    va_end(argv);

    DISPPARAMS dp = {NULL, NULL, 0, 0};
    dp.cArgs = argc;
    dp.rgvarg = pargs;
    if(type & DISPATCH_PROPERTYPUT)
    {
        DISPID t = DISPID_PROPERTYPUT;
        dp.cNamedArgs = 1;
        dp.rgdispidNamedArgs = &t;
    }

    hr = pdisp->Invoke(
        dispid,              // method or property name
        IID_NULL,            // may be always IDD_NULL
        LOCALE_USER_DEFAULT, // i dont know
        type,                // method(DISPATCH_METHOD) or property(DISPID_PROPERTYPUT)
        &dp,                 // args
        pret,                // return
        NULL,                // i dont know , ignore
        NULL);               // i dont know , ignore
    
    if(FAILED(hr))
    {
        printf("[!!!] failed to Invoke %s\n", name);
        return false;
    }
    printf("[*] success to Invoke %s\n", name);

    delete [] pargs;

    return true;
}