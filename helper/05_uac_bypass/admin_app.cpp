#include <iostream>
#include <string>
#include <fstream>

int main()
{
    std::cout << "try overwrite dat file\n";

    std::ofstream outputfile;
    std::string path_str = "";
    path_str = "C:/Windows/System32/test_log.dat";
    try
    {
        outputfile.open(path_str, std::ios::app);
        if (outputfile.fail()) 
        {
            std::cout << "failed open file." << std::endl;
        }
        outputfile << "test" << std::endl;
        outputfile.close();
        std::cout << "success open file." << std::endl;

    }
    catch(const std::ofstream::failure e)
    {
        std::cout << "failed open file. " << e.what() << std::endl;
    }
}