using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Linq;
using System.Diagnostics;

namespace test
{
    class RegistryOperator
    {
        static void Main(string[] args)
        {
            Console.WriteLine("create registry key");
            string path = "SOFTWARE\\TestWork\\Test";
            RegistryKey key = Registry.CurrentUser.CreateSubKey(path);
            string val = "C:\\Windows\\System32\\cmd.exe";
            key.SetValue("", val);
            
            path += "\\SubTest";
            key = Registry.CurrentUser.CreateSubKey(path);
            key.SetValue("neko", "hello");
            key.SetValue("kuma", "bye");
            key.Close();

            Console.ReadKey();

            Console.WriteLine("delete registry key");
            Registry.CurrentUser.DeleteSubKeyTree(path);

            Console.ReadKey();
        }
    }
}
