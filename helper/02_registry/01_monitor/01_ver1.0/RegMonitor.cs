// 参考URL
// https://dobon.net/vb/dotnet/system/registrykey.html

using System;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Threading;
using System.Collections.Generic;
using System.Collections;
using System.Xml.Linq;
using Microsoft.Win32;

public class RegMonitor
{
    private static readonly object pathLock = new object();

    private int maxRecurse = 99;
    private string pathReg = "";

    private Dictionary<string, Thread> dictThread = new Dictionary<string, Thread>();

    /// <summary>
    /// 実際のキー名でフォルダを作成していくとパスが長くなり、
    /// CreateDirectory()でPathTooLongExceptionを吐くため、擬似フォルダ名を使用する
    /// 本当の名前と擬似名を紐づけるディレクトリ
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    private Dictionary<string, string> coversionTable = new Dictionary<string, string>();

    public static string pathDir {private set; get; } = "";
    public static string pathTop {private set; get; } = "";
    public static string pathAbsMain {private set; get; } = "";
    
    /// <summary>
    /// ディレクトリを移動して、移動後ディレクトリで指定コールバックを行う
    /// </summary>
    /// <param name="path"></param>
    /// <param name="callback"></param>
    public static void ActionAfterChdir(List<string> path, Action callback = null)
    {
        lock(pathLock)
        {
            Directory.SetCurrentDirectory(pathAbsMain);
            foreach(var p in path)
            {
                if(!Directory.Exists(p)){
                    Console.WriteLine("[***] CreateDirectory, path: " + p);
                    Directory.CreateDirectory(p); 
                }
                // Console.WriteLine("[***] SetCurrentDirectory");
                Directory.SetCurrentDirectory(".\\" + p); 
            }
            callback?.Invoke();
            Directory.SetCurrentDirectory(pathAbsMain);
        }
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public RegMonitor(int maxRecurse = 99)
    {
        this.maxRecurse = maxRecurse;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public bool Initialize(string path)
    {
        if(!InitReg(path)){ return false; }

        pathReg = path;

        pathDir = ".\\results\\" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
        CreateDirecctory(pathDir);
        var splits = path.Split('\\');
        pathTop = splits[0];
        pathDir += "\\" + splits[0];
        CreateDirecctory(pathDir);
        
        Directory.SetCurrentDirectory(pathDir);
        pathAbsMain = System.IO.Directory.GetCurrentDirectory();

        Console.WriteLine("[*] start monitoring registry");
        return true;
    }

    /// <summary>
    /// dll内で対象レジストリキーを開く
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    private bool InitReg(string path)
    {
        if(!BaseDllImporter.init_reg(path)){ return false; }
        return true;
    }

    /// <summary>
    /// レジストリ監視結果を記録するファイルの保存ディレクトリを作成
    /// </summary>
    private void CreateDirecctory(string path)
    {
        Directory.CreateDirectory(path);
        Console.WriteLine("[*] make directory to {0}", path);
    }

    /// <summary>
    /// 対象レジストリを監視する
    /// </summary>
    public void MonitorReg(List<string> lstKeyName)
    {
        var info = new RegistryInfo(lstKeyName);
        
        if(info.Key == null){ return; }

        StartThreadMonitorReg(lstKeyName, info, () =>
        {
            OnChangeReg(info);
        });

        MonitorRegRecurse(info);
    }

    /// <summary>
    /// 対象レジストリを監視するスレッドを開始する
    /// </summary>
    public void StartThreadMonitorReg(List<string> lstKeyName, RegistryInfo info, BaseDllImporter.CallbackDelegate callback)
    {
        var keyName = "";
        foreach(var (k, idx) in lstKeyName.Select((k, idx) => (k, idx)))
        {
            if(idx == 0){ keyName += k; }
            else{ keyName += '\\' + k; }
        }

        lock (dictThread)
        {
            if(dictThread.ContainsKey(keyName)){ return; }
            
            Thread t = new Thread(new ThreadStart(() =>
            {
                BaseDllImporter.monitor_reg(() =>
                {
                    callback();
                });

                info.PostProcessing();
            }));
            t.Start();
            info.Init();
            dictThread.Add(keyName, t);
        }
    }

    /// <summary>
    /// レジストリ変更時のコールバック
    /// </summary>
    public void OnChangeReg(RegistryInfo info)
    {
        var flag = info.Update();
        if(flag)
        {
            MonitorRegRecurse(info);
        }
        else
        {
            lock (dictThread)
            {
                if(dictThread.ContainsKey(info.KeyName))
                {
                    info.PostProcessing(true);
                    dictThread[info.KeyName].Interrupt();
                    dictThread.Remove(info.KeyName);
                }
            }
        }
    }

    /// <summary>
    /// 再帰的にMonitorReg()を開始する
    /// </summary>
    /// <param name="keyName"></param>
    private void MonitorRegRecurse(RegistryInfo info)
    {
        List<string> lst = new List<string>();
        try
        {
            var subKeyNames = info.Key?.GetSubKeyNames();
            if(subKeyNames == null){ return; }

            foreach (var k in subKeyNames)
            {
                lst = new List<string>(info.LstKeyName);
                lst.Add(k);
                MonitorReg(lst);
            }   
        }
        catch (IOException e)
        {
            Console.WriteLine("[!!!] MonitorRegRecurse Err: " + e.GetType().Name); 
            Console.Write("      "); 
            foreach (var l in lst)
            {
                Console.Write("\\" + l);
            }
            Console.Write("\n");
        }
    }

    /// <summary>
    /// 生きているスレッドがあるかを確認する
    /// </summary>
    /// <returns>生きているスレッドがあればtrueを返す</returns>
    public bool IsExistAliveThread()
    {
        foreach (var item in dictThread)
        {
            if(item.Value.IsAlive){ return true;}
        }

        return false;
    }

    /// <summary>
    /// レジストリ監視終了時の後処理
    /// </summary>
    public void PostProcessing()
    {
        SetLoopFlagFalse();
        DummyReg();
    }

    /// <summary>
    /// ダミーのレジストリの作成、削除を行う
    /// dllのレジストリ監視用無限ループは、レジストリの変更を検知するまで、ロック状態になる
    /// ロック状態を開放するために、ダミーのレジストリの作成と削除を行う
    /// </summary>
    private void DummyReg()
    {
        string path = pathReg + "\\dummy";
        RegistryKey key = Registry.CurrentUser.CreateSubKey(path);
        Registry.CurrentUser.DeleteSubKeyTree(path);
    }

    /// <summary>
    /// レジストリ監視用無限ループのフラグを落とす
    /// </summary>
    private void SetLoopFlagFalse()
    {
        BaseDllImporter.set_loop_flage_false();
    }


    /// <summary>
    /// 後始末、dll内で対象レジストリキーを閉じる
    /// </summary>
    public void EndReg()
    {
        BaseDllImporter.end_reg();
    }

}

