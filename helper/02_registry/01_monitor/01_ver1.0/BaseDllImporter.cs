using System;
using System.Runtime.InteropServices;

public class BaseDllImporter
{
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CallbackDelegate();

    [DllImport("dll/reg_monitor.dll")]
    public extern static bool init_reg(string reg_name);

    [DllImport("dll/reg_monitor.dll")]
    public extern static void monitor_reg(CallbackDelegate callback);

    [DllImport("dll/reg_monitor.dll")]
    public extern static void end_reg();

    [DllImport("dll/reg_monitor.dll")]
    public extern static void set_loop_flage_false();

}