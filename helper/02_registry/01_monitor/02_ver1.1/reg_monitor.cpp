#include "reg_monitor.h"

bool __stdcall init_reg(const char *reg_name)
{
    const char *path = reg_name;
    if(RegOpenKeyEx(HKEY_CURRENT_USER, path, 0, KEY_NOTIFY, &hKey) != ERROR_SUCCESS)
    {
        printf("[!!!] failed to open registry %s\n", path);
        end_reg();
        return false;
    }
    printf("[*] success open registry %s.\n", path);
    return true;
}

// void __stdcall set_callback(callback_func c)
// {
//     callback = c;
// }

void __stdcall monitor_reg(callback_func callback)
{
    while(is_loop)
    {
        if(RegNotifyChangeKeyValue(hKey, TRUE, 
            REG_NOTIFY_CHANGE_NAME | 
            REG_NOTIFY_CHANGE_LAST_SET,
            NULL, FALSE) != ERROR_SUCCESS)
        {
            printf("[!!!] failed RegNotifyChangeKeyValue\n");
            break;
        }

        if(callback != NULL){ callback(); }
        else{ printf("[!!!] callback is null"); }
    }
}

void __stdcall end_reg()
{
    if(hKey != NULL){ RegCloseKey(hKey); }
    printf("[*] close\n");
}

void __stdcall set_loop_flage_false()
{
    is_loop = false;
}



