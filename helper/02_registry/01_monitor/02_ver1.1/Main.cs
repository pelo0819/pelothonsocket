using System;
using System.Linq;

public class MainRegMonitor
{
    static void Main(string[] args)
    {
        if(args.Length < 1)
        {
            Console.WriteLine("[!!!] input error.");
            return;
        }

        var maxRecurse = 99;
        if(args.Length >= 2 && Int32.TryParse(args[1], out int max))
        {
            maxRecurse = max;
        }
        RegMonitor monitor = new RegMonitor(maxRecurse);
        string path = args[0];
        var lst = path.Split('\\').ToList();
        if(!monitor.Initialize(path)){ return; }
        monitor.MonitorReg(lst);

        for(;;)
        {
            Console.Write(">>> ");
            var line = Console.ReadLine();
            if(line == "end")
            {
                monitor.PostProcessing();
                break; 
            }
        }

        for(;;)
        {
            if(!monitor.IsExistAliveThread()){ break; }
            // Console.WriteLine("wait until thread end");
        }
        monitor.EndReg();
    }
}
