using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Xml.Serialization;
using Microsoft.Win32;
using System.IO;
using System;

public class RegistryInfo
{
    public enum RegMode
    {
        None,
        Init,
        Add,
        Change,
        Delete,
        KeyDelete,
    };

    private readonly string defaultKeyName = "(default)";
    private int seq = 0;
    private string keyName = "";
    public string KeyName
    {
        set{ keyName = value; }
        get{ return keyName; }
    }

    /// <summary>
    /// 対象のレジストリキーまでのパスリスト
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <returns></returns>
    private List<string> lstKeyName = new List<string>();
    public List<string> LstKeyName
    {
        get{ return lstKeyName;}
    }

    private RegistryKey key = null;
    public RegistryKey Key
    {
        get{ return key; }
    }

    private Dictionary<string, string> values = new Dictionary<string, string>();

    private string fileName = "";


    private List<string> lstPath = new List<string>();

    public RegistryInfo(List<string> lstKeyName)
    {
        foreach(var (k, idx) in lstKeyName.Select((k, idx) => (k, idx)))
        {
            if(idx == 0){ keyName += k; }
            else{ keyName += '\\' + k; }
        }

        this.lstKeyName = new List<string>(lstKeyName);
        key = Registry.CurrentUser.OpenSubKey(keyName);
        SetLstPath(lstKeyName);
        CreateDirectory();
        SetFileName(lstKeyName);
    }


    public void Init()
    {
        if(key == null)
        {
            Console.WriteLine("{0} key is null.", keyName); 
            return; 
        }

        string[] valueNames = key.GetValueNames();
        foreach (var vn in valueNames)
        {
            var v = key.GetValue(vn).ToString();
            var keyName = vn == "" ? defaultKeyName : vn;
            values.Add(keyName, v);
            Output(RegMode.Init, keyName, v);
        }
    }


    private void SetFileName(List<string> lstKeyName)
    {
        var path = ".";
        foreach (var l in lstPath)
        {
            path += '\\' + l;
            Directory.CreateDirectory(path);
        }
        fileName = path + '\\' + RegMonitor.ConvertString(lstKeyName[lstKeyName.Count()-1]) + ".xml";
    }


    private void CreateDirectory()
    {
        var path = ".";
        foreach (var l in lstPath)
        {
            path += '\\' + l;
            Directory.CreateDirectory(path);
        }
    }

    /// <summary>
    /// lstPathをセットする
    /// lstPathはトップドメインから対象ディレクトリまでのディレクトリリスト
    /// </summary>
    /// <param name="lstKeyName"></param>
    private void SetLstPath(List<string> lstKeyName)
    {
        var buffer = new List<string>(lstKeyName);
        buffer.Remove(RegMonitor.pathTop);
        if(buffer.Count() > 0)
        {
            buffer.RemoveAt( buffer.Count() - 1);
        }

        foreach(var b in buffer)
        {
            lstPath.Add(RegMonitor.ConvertString(b));
        }
    }

    public bool Update()
    {
        key = Registry.CurrentUser.OpenSubKey(keyName);
        if(key == null){ return false; }

        var lstDelete = new List<string>();
        try
        {
            string[] valueNames = key?.GetValueNames();
            if(valueNames != null)
            {
                foreach (var vn in valueNames)
                {
                    var v = key.GetValue(vn).ToString();
                    var keyName = vn == "" ? defaultKeyName : vn;
                    if(!values.ContainsKey(keyName)) // 追加
                    {
                        values.Add(keyName, v);
                        Output(RegMode.Add, keyName, v);
                    }
                    else // 変更
                    {
                        var old = values[keyName];
                        if(old != v)
                        {
                            values[keyName] = v;
                            Output(RegMode.Change, keyName, v, old);
                        }
                    }
                }

                foreach(var vn in values)
                {
                    var keyName = vn.Key == defaultKeyName ? "" : vn.Key;
                    if(!valueNames.ToList().Contains(vn.Key))
                    {
                        lstDelete.Add(keyName);
                    }
                }
            }
        }
        catch(IOException e)
        {
            Console.WriteLine("[!!!] Update Err: " + e.GetType().Name);
            return false;
        }

        foreach(var v in lstDelete)
        {
            if(!values.ContainsKey(v)){ continue; }
            Output(RegMode.Delete, v, values[v]);
            values.Remove(v);
        }
        return true;
    }

    /// <summary>
    /// レジストリキーのバリュー情報を出力する
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="valueName"></param>
    /// <param name="value"></param>
    /// <param name="oldValue"></param>
    public void Output(RegMode mode, string valueName, string value = null, string oldValue = null)
    {
        if(mode == RegMode.None){ return; }
        if(fileName == ""){ 
            Console.WriteLine("[!!!] fileName not set.");
            return; 
        }
        
        var time = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        RegistryInfoXml serialize = new RegistryInfoXml();
        serialize.seq = seq.ToString();
        serialize.time = time;
        serialize.mode = mode.ToString();

        if(mode != RegMode.KeyDelete)
        {
            serialize.value = mode == RegMode.Change ? 
                "valueName: " + valueName + ", value: " + oldValue + " -- " + value :
                "valueName: " + valueName + ", value: " + value;
        }
        else
        {
            serialize.value = valueName;
        }
        
#if false
        RegMonitor.ActionAfterChdir(lstPath, ()=>
        {
            XmlSerializer se = new XmlSerializer(typeof(RegistryInfoXml));
            StreamWriter sw = new StreamWriter(fileName, true, new System.Text.UTF8Encoding(false));
            se.Serialize(sw, serialize);
            sw.Close();
        });
#else
        XmlSerializer se = new XmlSerializer(typeof(RegistryInfoXml));
        StreamWriter sw = new StreamWriter(fileName, true, new System.Text.UTF8Encoding(false));
        se.Serialize(sw, serialize);
        sw.Close();
#endif
        seq++;
    }

    public void PostProcessing(bool isDelete = false)
    {
        if(isDelete){ Output(RegMode.KeyDelete, "delete key.");}
        
        key = Registry.CurrentUser.OpenSubKey(keyName);
        if(key == null){ return; }
        key.Close();

    }

}