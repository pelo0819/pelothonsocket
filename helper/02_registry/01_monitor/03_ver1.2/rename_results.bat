@echo off

set root=%~dp0

set folder=9999_99_99_99_99_99
set from=tmp_results\
set to=results\

set org="%from%%folder%"
set copy="%to%%folder%"
REM ディレクトリのコピーは質問来るのでechoを挟んで答えるようにする
echo d|xcopy /e %org% %copy%

cd %p%

cd %root%
call :get_value z
call :get_folder_list
call :rename
exit /b

REM 引数(key)よりdict_conversion内のvalueを取得するサブルーチン
:get_value
for /f "tokens=1,2" %%a in (dict_conversion) do (
    set ret=null
)&if %%a equ %1 (
    set ret=%%b
    goto :break
)
exit /b

REM 擬似break用のサブルーチン
:break
exit /b

:get_folder_list
setlocal enabledelayedexpansion
set num=0
for /f "tokens=1-5" %%a IN ('dir') do (
    if %%c equ ^<DIR^> (
        set /a num=num+1
        REM dirの出力結果.と..は無視する
        if !num! gtr 2 (
            echo %%d
        )
    )
)
exit /b

:rename
set p=abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb
REM rename aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\\aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.txt ff.txt
rename %p%\\aaaaaaaaaaaaaaaaaaaaaaaaaaaa.txt ff.txt
@REM cd 

exit /b
