using System;
using System.Linq;

public class MainRegMonitor
{
    static void Main(string[] args)
    {
        if(args.Length < 1)
        {
            Console.WriteLine("[!!!] input error.");
            return;
        }

        RegMonitor monitor = new RegMonitor();
        string path = args[0];
        var lst = path.Split('\\').ToList();
        if(!monitor.Initialize(path)){ return; }
        monitor.MonitorReg(lst);

        for(;;)
        {
            Console.Write(">>> ");
            var line = Console.ReadLine();
            if(line == "end")
            {
                monitor.PostProcessing();
                break; 
            }
        }

        for(;;)
        {
            if(!monitor.IsExistAliveThread()){ break; }
            // Console.WriteLine("wait until thread end");
        }
        monitor.Finalie();
    }
}
