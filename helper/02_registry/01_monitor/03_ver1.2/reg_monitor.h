#pragma once

#include <windows.h>
#include <stdio.h>
#include <winreg.h>

#pragma comment(lib, "Advapi32.lib")

using callback_func = void(*)();

HKEY hKey = NULL;
// callback_func callback;
bool is_loop = true;

bool __stdcall init_reg(const char *reg_name);

// void __stdcall set_callback(callback_func callback);

void __stdcall monitor_reg(callback_func callback);

void __stdcall end_reg();

void __stdcall set_loop_flage_false();

