#include <windows.h>
#include <stdio.h>
#include <winreg.h>
#include <string>

#pragma comment(lib, "Advapi32.lib")

bool check_input(int argc);
bool init_reg(char *key_name);
void monitor_reg();
void close(HKEY hKey);

HKEY hKey = NULL;

int main(int argc, char **argv)
{
    if(!check_input(argc)){ return 1; }

    if(!init_reg(argv[1])){ return 1; }

    monitor_reg();

    close(hKey);
    return 0;
}

bool check_input(int argc)
{
    if(argc < 2)
    {
        printf("[!!!] input error.\n");
        return false;
    }
    printf("[*] start monitoring registry.\n");
    return true;
}

bool init_reg(char *key_name)
{
    const char *path = key_name;
    if(RegOpenKeyEx(HKEY_CURRENT_USER, path, 0, KEY_NOTIFY, &hKey) != ERROR_SUCCESS)
    {
        printf("[!!!] failed to open registry %s\n", path);
        close(hKey);
        return false;
    }
    printf("[*] success open registry %s.\n", path);
    return true;
}

void monitor_reg()
{
    for(;;)
    {
        if(RegNotifyChangeKeyValue(hKey, TRUE, 
            REG_NOTIFY_CHANGE_NAME | 
            REG_NOTIFY_CHANGE_LAST_SET,
            NULL, FALSE) != ERROR_SUCCESS)
        {
            printf("[!!!] failed RegNotifyChangeKeyValue\n");
            break;
        }
        printf("[*] change Registry\n");
        
    }
}

void close(HKEY hKey)
{
    if(hKey != NULL)
    {
        RegCloseKey(hKey);
    }
    printf("[*] close\n");
    getchar();
}