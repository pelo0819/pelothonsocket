echo off

@REM 引数は実行するexeファイルの絶対パス
@REM 参考url https://qiita.com/hkuno/items/e7fedc20a61979aa6078
@REM https://docs.microsoft.com/ja-jp/cpp/build/how-to-embed-a-manifest-inside-a-c-cpp-application?view=msvc-160
@REM exeファイル名
set file=%~n1
set ext=%~x1
@REM ドライバー名
set driver=%~d1
@REM パス名
set pass=%~p1

@REM 一応出力
echo file:%file%
echo driver:%driver%
echo pass:%pass%

@REM ディレクトリを移動
cd %driver%%pass%

set mypath=%~dp0pelo.xml

@REM call C:\Windows\Microsoft.NET\Framework64\v4.0.30319\csc.exe -win32manifest:%mypath% %file%
call cl %file%%ext%
pause
call mt -manifest ./pelo.xml -outputresource:%file%.exe;1