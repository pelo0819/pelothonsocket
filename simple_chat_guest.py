#! /usr/bin/env python

import socket
import threading
import sys
import argparse

is_loop = True
pre_input =""

def get_is_loop():
    global is_loop
    return is_loop

def set_is_loop(bflag):
    global is_loop
    is_loop = bflag

def ending(soc):
    set_is_loop(False)
    soc.close()
    sys.exit()


def host_recv_handler(soc):
    print("[*] host_recv_handler start.")
    try:
        while get_is_loop():
            r = soc.recv(4096)
            dr = r.decode('utf-8')
            print("\n<<<" + dr)
    except:
        pass

def handle_input(soc):
    try:
        while get_is_loop():
            i = input(">>>")
            soc.send(i.encode('utf-8'))
            if i =="--bye" or i == "--Bye":
                ending(soc)
    except:
        ending(soc)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--target", default="localhost", help="target ipAddress")
    parser.add_argument("-p", "--port", type=int, default = 8080,help="target port")
    args=parser.parse_args()

    target_host = args.target
    target_port = args.port

    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((target_host, target_port))

    host_recv_thread = threading.Thread(
        target = host_recv_handler,
        args= (client,))
    host_recv_thread.start()

    handle_input(client)


if __name__ =="__main__":
    main()