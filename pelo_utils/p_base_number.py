class p_base_number:
    def binarydump(self, src):
        print("--- Binary ---")
        length = 16
        result = []
        for i in range(0, len(src), length):
            s = src[i:i + length]
            bina = ' '.join(['{:08b}'.format(x, 'b') for x in s])
            result.append("%s " % bina)
        print('\n'.join(result))
    
    def hexdump(self, src, length = 16):
        print("--- Hexadecimal ---")        
        result = []
        digits = 4 if isinstance(src, bytes) else 2
        digits = 2

        for i in range(0, len(src), length):
            s = src[i:i + length]
            hexa = ' '.join(["%0*X" % (digits, x) for x in s])
            text = ''.join([chr(x) if 0x20 <= int(x) < 0x7F else '.' for x in s])
            result.append("%04X %-*s %s" % (i, length * (digits + 1), hexa, text))
        print('\n'.join(result))