import urllib.request, urllib.error
import threading
import os
import queue

threads = 10
target = "http://192.168.3.30/"
directory = "/Users/tobita/Documents/wordpress/wordpress"
filters = [".jpg", ".gif", ".png", ".css"]

os.chdir(directory)

web_paths = queue.Queue()

for curdir,dirs,files in os.walk("."):
    for f in files:
        remote_path = "%s%s" % (curdir, f)
        if remote_path.startswith("."):
            remote_path = remote_path[1:]
        if os.path.splitext(f)[1] not in filters:
            web_paths.put(remote_path)

def test_remote():
    while not web_paths.empty():
        path = web_paths.get()
        url = "%s%s" % (target, path)
        # print("url = %s" % url)
        request = urllib.request.Request(url)

        try:
            with urllib.request.urlopen(request) as r:
                print("[%d] => %s" % (r.code, path))
        except urllib.error.HTTPError as e:
            print(e.code)
        except urllib.error.URLError as e:
            print(e.reason)

for i in range(threads):
    print("Spawning thread: %d" % i)
    t = threading.Thread(target = test_remote)
    t.start()


# heeders = {}
# heeders['User-Agent'] = 'Googlebot'

# url2 = "%s%s" % (target, "index2.php")
# request2 = urllib.request.Request(target, headers=heeders)

# try:
#     with urllib.request.urlopen(request2) as res:
#         print("[%d] => %s" % (res.code, url2))
# except urllib.error.HTTPError as err:
#     print(err.code)
# except urllib.error.URLError as err:
#     print(err.reason)
