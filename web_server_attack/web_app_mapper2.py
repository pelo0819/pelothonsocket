import queue
import threading
import os
import urllib

threads = 10
target = "http://192.168.3.30"
directory = "/Users/tobita/Documents/pelothonsocket"

os.chdir(directory)

web_paths = queue.Queue()

for curdir, dirs, files in os.walk("."):
    for f in files:
        path = "%s/%s" % (curdir, f)
        if curdir.startswith("."):
            path = path[1:]
        if os.name == "nt":
            code = 92
        else:
            code = 165
        path = path.replace(chr(code), "/")
        print("%s" % path)