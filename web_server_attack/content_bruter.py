import urllib.request, urllib.error
import threading
import os
import queue

threads = 10
target = "http://192.168.3.30"
user_agent = "Mozilla/5.0 (X11; Linux x86_64; rv:19.0) Gecko/20100101Firefox/19.0"
wordlist_file = "./resources/all.txt"
# wordlist_file = "./resources/test_list.txt"
resume = None

def build_wordlist(wordlist_file):
    with open(wordlist_file) as fd:
        raw_words = fd.readlines()
        found_resume = False
        words = queue.Queue()
        for w in raw_words:
            w = w.rstrip()
            if resume is not None:
                if found_resume:
                    words.put(w)
                else:
                    if w == resume:
                        found_resume = True
                        print("Resuming wordlist from: %s" % resume)
            else:
                words.put(w)
        return words

def dir_bruter(word_queue, extensions = None):
    while not word_queue.empty():
        attempt = word_queue.get()

        attempt_list = []

        if "." not in attempt:
            attempt_list.append("/%s/" % attempt)
        else:
            attempt_list.append("/%s" % attempt)
        
        if extensions:
            for extension in extensions:
                attempt_list.append("/%s%s" % (attempt, extension))
        
        for brute in attempt_list:
            url = "%s%s" % (target, urllib.request.quote(brute))
            
            try:
                headers = {}
                headers["User-Agent"] = user_agent
                request = urllib.request.Request(url, headers=headers)
                with urllib.request.urlopen(request) as res:
                    if len(res.read()):
                        print("[%d] => %s" % (res.code, url))
            except urllib.error.HTTPError as e:
                if hasattr(e, 'attr') and e.code != 404:
                    print("[%d] => %s" % (res.code, url))
            except urllib.error.URLError as e:
                # print(e.reason)
                pass



extensions = {".php", ".bak", ".inc"}
words = build_wordlist(wordlist_file)

for i in range(threads):
    t = threading.Thread(target=dir_bruter, args=(words, extensions,))
    t.start()