import urllib.request, urllib.error
import threading
import os
import queue

threads = 10
target = "http://192.168.3.30/"
directory = "/Users/tobita/Documents/wordpress/wordpress"
filters = [".jpg", ".gif", ".png", ".css"]

heeders = {}
heeders['User-Agent'] = 'Googlebot'

url = "%s%s" % (target, "index2.php")
req = urllib.request.Request(url, headers=heeders)

try:
    with urllib.request.urlopen(req) as res:
        print(res.read())
        print("[%d] => %s" % (res.code, url))
except urllib.error.HTTPError as err:
    print(err.code)
except urllib.error.URLError as err:
    print(err.reason)
