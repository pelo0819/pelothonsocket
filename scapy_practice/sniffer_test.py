# -*- coding: utf-8 -*-
#! /usr/bin/env python

from scapy.all import *

def packet_callback(packet):
    if packet[TCP].payload:
        recv_packet = str(packet[TCP].payload)
        if "hello" in recv_packet:
            print("[*] sender ip %s" % packet[IP].dst)
            print("[*] receiver ip %s" % packet[IP].src)
    # print(packet.show())

def main():
    sniff(filter="tcp", prn = packet_callback, store = 0)


if __name__ == "__main__":
    main()