# -*- coding: utf-8 -*-

import cv2

img = cv2.imread("./neko.jpeg")

gray = cv2.cvtColor(img, cv2.COLOR_RGBA2GRAY)

cv2.imshow("gray", gray)

while True:
    if cv2.waitKey(1) == 27:
        break

cv2.destroyAllWindows()