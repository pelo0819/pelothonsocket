from ctypes import cdll
from ctypes import c_char_p
import ctypes
import time
import threading
# from multiprocessing import process
from multiprocessing import Process, Pipe
import pipes

dll = cdll.LoadLibrary('./pelo_dll.dll')
#dll = cdll.LoadLibrary('C:/Users/tobita/Documents/Pelows/pelo_dll/x64/Release/pelo_dll.dll')
isLooping = True
parent_conn, child_conn = Pipe()

def hook_process(connection):
    dll.init()
    dll.startHook()
    while True:
        instance = connection.recv()
        if instance:
            dll.endHook()
            print("[*] end hook")
            return

def manage_hook():
    child = Process(target=hook_process, args=(child_conn,))
    child.start()
    isHooking = True
    while isLooping:
        time.sleep(5)
        isEnableWindow = dll.isCurrentWindowEnableHook()
        if not isEnableWindow and isHooking:
            item = 1
            parent_conn.send(item)
            isHooking = False
            print("[!!!] unable hook windos, pause Hook")
        if isEnableWindow and not isHooking:
            child = Process(target=hook_process, args=(child_conn,))
            child.start()
            print("[*] restart hook")
            isHooking = True

def set_isLooping(flag):
    global isLooping
    isLooping = flag

if __name__ == '__main__':
    print("HELOO LETS KEY HOOOK !!")
    for i in range(5):
        t = 5 -i
        print("%d" % t)
        time.sleep(1)
    print("S T A R T !!")    
    th = threading.Thread(target=manage_hook)
    th.start()
    while isLooping:
        inp = input()
        if "stop" in inp:
            print("stop")
            set_isLooping(False)
    th.join()
    item = 1
    parent_conn.send(item)
    print("[*] finish hook")
    print("!! BYE-BYE !!")


        