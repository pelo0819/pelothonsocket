# -*- coding: utf-8 -*-
import urllib.request, urllib.error, urllib.parse
import re
from html.parser import HTMLParser
import argparse
import os

curr_dir = os.getcwd()
print(curr_dir)

file_name = "word_list.txt"

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--path", type = str, default = file_name)
args = parser.parse_args()

file_name = args.path

class TagStripper(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.page_text = []

    def handle_data(self, data):
        self.page_text.append(data)
    
    def handle_comment(self, data):
        self.handle_data(data)
    
    def run(self, html):
        self.feed(html)
        return " ".join(self.page_text)


def mangle(word):
    suffixies = [""]
    mangled = []

    for pwd in (word, word.capitalize()):
        for suffix in suffixies:
            mangled.append("%s%s" % (pwd, suffix))
    
    return mangled

# url = "http://192.168.3.50"
url = "https://www.javadrive.jp/python/string/index12.html"

handler = urllib.request.HTTPHandler()
opener = urllib.request.build_opener(handler)

word_list =[]
with opener.open(url) as res:
    r = res.read()
    if len(r):
        print("[%d] => %s" % (res.code, url))
    print(r.decode('utf-8'))
    parser = TagStripper()
    page_text = parser.run(r.decode('utf-8'))

    words = re.findall("[a-zA-Z]\w{2,}", page_text)
    for word in words:
        if len(word) <= 12:
            word_list.append(word.lower())


with open(file_name, mode = 'w') as file:
    for w in word_list:
        for m in mangle(w):
            file.write("%s\n" % m)
