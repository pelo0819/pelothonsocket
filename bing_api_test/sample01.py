#Copyright (c) Microsoft Corporation. All rights reserved.
#Licensed under the MIT License.

# -*- coding: utf-8 -*-

import json
import os 
from pprint import pprint
import requests
import socket

# Add your Bing Search V7 subscription key and endpoint to your environment variables.
subscription_key = "36e45cef19f34491882de98f237836d9"
search_url = "https://daijiro.cognitiveservices.azure.com/bing/v7.0/search"

# Construct a request
headers = { 'Ocp-Apim-Subscription-Key': subscription_key }
# params = {'q': search_term, 'textDecorations': True, 'textFormat': 'HTML'}

target_url = 'http://testphp.vulinweb.com/'
search_ip = '176.28.50.165'
#search_ip = '175.184.47.17'
query_string = "ip:%s" % search_ip
params = {'q': query_string}

# Call the API
try:
    response = requests.get(search_url, headers=headers, params=params)

    print("\nrequest URl\n")
    print(response.url)

    print("\nHeaders:\n")
    print(response.headers)

    j = response.json()

    print("\nJSON Response:\n")
    # pprint(j)

    # json_parse = json.loads(j)

    ff = j["webPages"]["value"]
    # print(ff)
    print("len ff: %d" % len(ff))
    print("type ff: %s" % type(ff))

    for i in ff:
        # print("*"* 100)
        # print("type i %s" % type(i))
        # print(i)
        if "url" in i:
            print(i["url"])








except Exception as ex:
    raise ex
