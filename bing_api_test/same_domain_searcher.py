# -*- coding: utf-8 -*-
import json
import os 
import requests
import socket
import argparse
from pprint import pprint


parser = argparse.ArgumentParser()
parser.add_argument("-p", "--path", type = str, default = "C:/pelo/bing_key.json")
parser.add_argument("-i", "--ip", type = str, default = "localhost")
parser.add_argument("-sh", "--showhead", type = bool, default = False)
parser.add_argument("-sb", "--showbody", type = bool, default = False)
args = parser.parse_args()

# set json file path
path = args.path
# set search url
search_host = args.ip
# set show json body
is_show_header = args.showhead
is_show_body = args.showbody

# load bing api key and url
json_flie = open(path)
json_obj = json.load(json_flie)

tag_key = "key"
tag_url = "url"

subscription_key = ""
search_bing_url = ""

if tag_key in json_obj:
    subscription_key = json_obj[tag_key]
if tag_url in json_obj:
    search_bing_url = json_obj[tag_url]

if subscription_key == "" or search_bing_url == "":
    print("[!!!] this json file dont have bing api info.")
    exit(0)

headers = { 'Ocp-Apim-Subscription-Key': subscription_key }

query_string = "ip:%s" % search_host
params = {'q': query_string}

try:
    response = requests.get(search_bing_url, headers=headers, params=params)

    print("[*] reques => %s" % response.url)

    if is_show_header:
        print("[*] headers:")
        print(response.headers)

    resopnse_json = response.json()

    if is_show_body:
        print("[*] json body:")
        pprint(resopnse_json)

    value_dict = resopnse_json["webPages"]["value"]
    for u in value_dict:
        if "url" in u:
            print("[*] url: %s" % u["url"])

except Exception as ex:
    print("[!!!] error occoured by bing api.")
