import sys
import socket
import getopt
import threading
import subprocess

# if Debug mode , true
is_debug = False

listen = False
command = False
upload = ""
execute = ""
target = ""
upload_destination = ""
port = 0

def  usage():
    print("BHP Net Tool")
    print()
    print("Usage : bhpnet.py -t targert_host -p port")
    print("-l --listen                 - listen on [host]:[port] for")
    print("                              incoming connection")
    print("-e --execute=file_to_run    - execute the given file upon")
    print("                              recieving a connection")
    print("-c --command                - initialize a command shell")
    print("-u --upload=destination     - upon recieving connection upload a")
    print("                              file and write to [destination]")
    print()
    print("Examples:")
    print("bhpnet.py -t 192.168.0.1 -p 5555 -l -c")
    print("bhpnet.py -t 192.168.0.1 -p 5555 -l -u c:\\target.exe")
    print("bhpnet.py -t 192.168.0.1 -p 5555 -l -e \"cat /etc/passwd\"")
    print("echo 'ABCDEFGH' | ./bhpnet.py -t 192.168.11.12 -p 135")
    sys.exit(0)
    
def client_sender(buffer):
    print("cliend_sender")
    print(buffer)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    try:
        client.connect((target, port))
        
        if len(buffer): # length of buffer is positive.
            client.send(buffer)

        while True:
            recv_len = 1
            response = ""
            while recv_len:
                data = client.recv(4096)
                recv_len = len(data)
                response += data.decode('utf-8')
                if recv_len < 4096:
                    print("recv_len is shorter than 4096")
                    break
            
            print(response)
            buffer = input("")
            buffer += "\n"
            client.send(buffer.encode('utf-8'))
    except:
        print("[*] Exception Existing")
        client.close()
    
def server_loop():
    global target
    if not len(target):
        target ="0.0.0.0"
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind((target, port))
    server.listen(5)
    print("server_loop start!!!")
    while True:
        client_socket, addr = server.accept()
        client_thread = threading.Thread(
            target = client_handler, args = (client_socket,))
        client_thread.start()

def run_command(command):
    command = command.rstrip()
    try:
        output = subprocess.check_output(
            command, stderr = subprocess.STDOUT, shell = True)
    except:
        output = "Failed to excute command.\r\n"
    return output

def client_handler(client_socket):
    print("client_handler start")
    global upload
    global execute
    global command
    
    if len(upload_destination):
        file_buffer = ""
        while True:
            data = client_socket.recv(1024)
            if len(data) == 0:
                break
            else:
                file_buffer += data
        
        try:
            file_descriptor = open(upload_destination, "wb")
            file_descriptor.write(file_buffer)
            file_descriptor.close()
            client_socket.send(
            "Successfully saved file to %s\r\n" % upload_destination)
        except:
            client_socket.send(
            "Failed saved file to %s\r\n" % upload_destination)            
    
    if len(execute):
        output = run_command(execute)
        client_socket.send(output)
    
    if command:
        prompt = '<HackPelo:#>'
        client_socket.send(prompt.encode('utf-8'))
        while True:
            cmd_buffer = ""
            while "\n" not in cmd_buffer:
                data = client_socket.recv(1024)
                cmd_buffer += data.decode('utf-8')
            response = run_command(cmd_buffer)
            response += '<HackPelo:#>'
            client_socket.send(response)

def main():
    global listen
    global port
    global execute
    global command
    global upload_destination
    global target
    if not len(sys.argv[1:]):
        usage()
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            # "h:l:e:t:p:c:u:",
            "hle:t:p:cu:",
            ["help", "listen", "execute=", 
             "targets=", "ports=", "command=", "upload="]
        )
        print("Options:", opts)
        print("Argments", args)        
    except getopt.GetoptError as err:
        print(str(err))
        usage()
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
        elif o in ("-l", "--listen"):
            listen = True
        elif o in ("-e", "--execute"):
            execute = a
        elif o in ("-c", "--command"):
            command = True         
        elif o in ("-u", "--upload"):
            upload_destination = a
        elif o in ("-t", "--target"):
            target = a
        elif o in ("-p", "--port"):
            port = int(a)
        else:
            assert False, "Unhandled Option"
    #check input value 
    print("listen=%s, execute=%s, command=%s, upload_destination=%s, target=%s, port=%s"
          % (listen, execute, command, upload_destination, target, port))
    # not listen mode and length of target(ip Address) is positive, port is positive
    if not listen and len(target) and port > 0:
        buffer = sys.stdin.read()
        #buffer = input(">>>")
        client_sender(buffer)
    if listen:
        server_loop()

#main loop start
main()