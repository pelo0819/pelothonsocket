
import socket
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--target", default = "localhost", help = "target ipAddress")
    parser.add_argument("-p", "--port", type = int, default = 8080,help = "target port")
    args=parser.parse_args()

    target_host = args.target
    target_port = args.port

    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        client.sendto("hello".encode('utf-8'), (target_host, target_port))
        print("send packet to %s:%s" % (target_host, target_port))
        data, addr = client.recvfrom(4096)
    except:
        print("failed send udp packet to %s" % ip)
        pass

    print("%s" % data.decode('utf-8'))


if __name__ =="__main__":
    main()