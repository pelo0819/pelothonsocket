import re

def print_split(string, pattern):
    ss = re.findall(pattern, string)
    print(ss)
    if not ss is None or len(ss) > 0:
        dd = dict(ss)
        for k, v in dd.items():
            print("key:%s, value:%s" % (k, v))
        print("")
    else:
        print("[!!!] Nothing matches pattern.")


def print_dict(d):
    if not isinstance(d, dict):
        return
    for k, v in d.items():
        print("key:%s, value:%s" % (k,v))

s1 = """
ab a : cdeaa:f2222222: hiiiiaj : kl:am::nnaopaq
"""
pat1 = "a(?P<name>.*): (?P<value>.*)a"
match1 = re.findall(pat1, s1)
dict1 = dict(match1)
print_dict(dict1)

s2 = "123ad456b789"
pat2 = "([0-9]+)([a-zA-Z]+)([0-9]+)"
match2 = re.findall(pat2, s2)
print(match2)

# s3 = "123ad456b789"
# pat3 = "(?P<name>[0-9]+)([a-zA-Z]+)(?P<value>[0-9]+)"
# match3 = re.findall(pat2, s2)
# dict3 = dict(match3)
# print(match3)
# print_dict(dict3)

s4 = "123ad456sb789"
pat4 = "([0-9]+?)([a-zA-Z]{2})([0-9]+?)"
match4 = re.findall(pat4, s4)
print(match4)


s5 = "aaaaabcdefag"
pat5 = "(.*)a"
match5 = re.findall(pat5, s5)
print(match5)


s6 = "あいうえbcdg"
pat6 = "[ぁ-ん]{4}"
match6 = re.findall(pat6, s6)
print(match6)

s7 ="s."
pat7 = "^\w+([-+.]\w+)*"
match7=re.match(pat7, s7)
if match7:
    print("match")
else:
    print("dismatch")

s8 ="+.--"
pat8 = "^[-+.]{3}$"
match8=re.match(pat8, s8)
if match8:
    print("match")
else:
    print("dismatch")